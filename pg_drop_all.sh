#!/usr/bin/env bash
# Drop all data from a postgresql database, but leave schema intact

echo "Dropping sequences..."
psql -t -c "select 'drop sequence ' || sequence_name || ' cascade;' from information_schema.sequences where sequence_schema='public'" | psql

echo "Dropping tables..."
psql -t -c "select 'drop table '    || table_name    || ' cascade;' from information_schema.tables where table_schema='public'" | psql

echo "Done."
