'''
@author: Richard Plevin
@author: Sam Fendell

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
from __future__ import with_statement

import re
import os.path
from inspect import stack, getargspec

from . import log
from .error import BaseSpecError, ShellCommandError, CoreMcsUserError, CoreMcsSystemError
from .Configuration import getConfigManager
from .Constants import COMMENT_CHAR, PARAM_SUFFIX

_logger = log.getLogger(__name__)


class Context(object):
    def __init__(self, simId, trialNum, expName, appName, jobNum=None):
        self.simId    = simId
        self.trialNum = trialNum
        self.expName  = expName
        self.jobNum   = jobNum
        self.appName  = appName

    def __str__(self):
        return "App=%s Exp=%s simId=%s trialNum=%s job=%s" % \
               (self.appName, self.expName, self.simId, self.trialNum, self.jobNum)

def getTempFile(suffix, text=True):
    from tempfile import mkstemp

    cfg = getConfigManager()
    tmpDir = cfg.getParam('Core.TmpDir', default='/tmp')

    if not os.path.isdir(tmpDir):
        mkdirs(tmpDir)  # ignores error if already exists

    fd, tmpFile = mkstemp(suffix=suffix, dir=tmpDir, text=text)
    os.close(fd)    # we don't need this; we'll open the file later

    return tmpFile

def getJobNum():
    cfg = getConfigManager()
    batchSystem = cfg.getParam('Core.BatchSystem')
    job_id_var  = cfg.getParam("%s.%s" % (batchSystem, 'JOB_ID_VAR'))
    jobIdStr  = os.getenv(job_id_var, '')

    result = re.search('\d+', jobIdStr)
    jobNum = int(result.group(0)) if result else 999999
    return jobNum

def jobTmpDir():
    '''
    Generate the name of a temporary directory based on the value of $MCS_TMP (if set,
    or '/tmp' if not) and the job ID from the environment.
    '''
    cfg = getConfigManager()
    tmpDir = cfg.getParam('Core.TmpDir', default='/tmp')
    dirName = "mcs.%s.tmp" % getJobNum()
    dirPath = os.path.join(tmpDir, dirName)
    return dirPath

def tail(filename, count):
    '''
    Return the last `count` lines from a text file by running the
    tail command as a subprocess. (Faster than native Python...)

    :param filename: a filename
    :param count: number of lines to read from end of file
    :return: a list of the lines read
    '''
    from subprocess import check_output
    pipe = None

    try:
        cmd = 'tail -n %d %s' % (count, filename)
        lines = check_output(cmd, shell=True)

    except Exception, e:
        msg = "Failed run 'tail' on file '%s': %s" % (filename, e)
        raise CoreMcsSystemError(msg)
    finally:
        if pipe:
            pipe.close()

    return lines

LogLinesToSave = 100
QuarterGigabyte = 1024 * 1024 * 128

def truncateBigFile(filename, maxSize=QuarterGigabyte, linesToSave=LogLinesToSave, delete=True):
    '''
    If the named file is greater than maxSize (default 128 MB), read the last
    `count` lines (default 100) from the file, write the saved lines in a
    file with new extension of .truncated.out, and if `delete` is non-False,
    delete the original file.
    '''
    try:
        fileSize = os.path.getsize(filename)
        if fileSize <= maxSize:
            return
    except Exception, e:
        msg = "Failed to get size of file '%s': %s" % (filename, e)
        raise CoreMcsSystemError(msg)

    text = tail(filename, linesToSave)

    base, ext = os.path.splitext(filename)
    newName = base + '.truncated' + ext

    with open(newName, "w") as f:
        f.write(text)

    if delete:
        os.unlink(filename)

def computeLogPath(simId, expName, logDir, trials):
    trialMin = min(trials)
    trialMax = max(trials)
    trialRange = trialMin if trialMin == trialMax else "%d-%d" % (trialMin, trialMax)
    jobName  = "%s-s%d-%s" % (expName, simId, trialRange)   # for displaying in job queue
    logFile  = "%s-%s.out" % (expName, trialRange)          # for writing diagnostic output
    logPath  = os.path.join(logDir, logFile)
    return logPath, logFile, jobName

# Remember the running package so database can access "initial data".
# Can't do this in CorePackage because it creates an import cycle.
_RunningPackage = None

def setRunningPackage(pkg):
    global _RunningPackage
    _RunningPackage = pkg

def getRunningPackage():
    return _RunningPackage

def runShellCommand(exe, args, shell=True, outfile=None, verbose=False):
    '''
    Run a command either in a shell or directly as a subprocess. If shell is
    True, args should be a string with the desired arguments, and outfile can
    specify where to direct the output of the command. If shell is False, args
    should be a list of arguments or None, and outfile is ignored. In all cases,
    exe should be a string with the path of the program to execute.
    '''
    from subprocess import call     # lazy import

    if shell:
        if args is None:
            args = ""

        cmd = "%s %s > %s" % (exe, args, outfile) if outfile else "%s %s" % (exe, args)
    else:
        # 'args' should be a list or None
        cmd = [exe]
        if args:
            cmd += args

    cmdStr = cmd if shell else ' '.join(cmd)
    if verbose:
        print cmdStr

    status = -1 # in case call raises an error
    try:
        status = call(cmd, shell=shell)

    except OSError, e:
        if e.errno == 2:
            raise ShellCommandError("No such file: '%s'" % exe, exitStatus=status)

    except Exception, e:
        raise ShellCommandError("Failure invoking %r: %s" % (cmdStr, e), exitStatus=status)

    if status != 0:
        raise ShellCommandError("program '%s' failed" % exe, exitStatus=status)

def sign(number):
    return cmp(number, 0)

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def checkSuffix(path, suffix):
    return os.path.splitext(path)[1][1:] == suffix

def fileReader(filename, error=BaseSpecError, fileExtension=None, split=True):
    '''
    Generator for reading a file line by line.

    error is an exception class that subclasses BaseSpecError. lineNum is changed
    automatically as the generator reads the file to ensure that any error thrown
    has the correct line number printed.

    fileExtension is the expected extension of the file. If the
    extension doesn't match, an exception is thrown.

    split is a boolean indicating whether the line should be tokenized or not.

    Returns either a list of tokens in a line (if split is True) or a single string.
    '''
    error.filename = filename.replace('\\', '/')

    if fileExtension:
        if not checkSuffix(filename, fileExtension):
            raise error('Wrong file extension. Should be "%s"' % fileExtension)

    with open(filename, 'r') as f:
        for lineNum, line in enumerate(f):
            error.lineNum = lineNum + 1
            line = line.split(COMMENT_CHAR)[0].strip()  # allow comments
            if len(line) == 0:
                continue
            if split:
                line = line.split()
            yield line

def getOptionalArgs(func):
    '''
    Inspects a function to find the names of optional arguments.
    Used in distrogen to generate distrogens from a single function.

    func is a function object.
    Returns a tuple of strings of the optional argument names.
    '''
    args, _, _, defaults = getargspec(func)
    args = args[-len(defaults):]
    return args


def printMat(m):
    '''
    Version of matrix printing that labels rows and columns.
    '''
    if m is None:
        return

    count = m.shape[0]
    print '      ',
    for i in range(count):
        print '%5d ' % i,
    print ''

    for i in range(count):
        print  '%5d ' % i,
        for j in range(count):
            print '%5.2f ' % m[i, j],
        print ''

def loadModuleFromPath(modulePath):
    '''
    The load a module from a '.py' or '.pyc' file from a path that ends in the
    module name, i.e., from "foo/bar/Baz.py", the module name is 'Baz'.
    '''
    from imp import load_source, load_compiled  # lazy import to speed startup

    # Extract the module name from the module path
    base       = os.path.basename(modulePath)
    moduleName = base.split('.')[0]

    _logger.info('loading module %s' % base)

    # Load the compiled code if it's a '.pyc', otherwise load the source code
    module = None
    try:
        if base.endswith('.py'):
            module = load_source(moduleName, modulePath)
        elif base.endswith('.pyc'):
            module = load_compiled(moduleName, modulePath)
        else:
            raise Exception('Unknown module type (%s): file must must be .py or .pyc' % modulePath)

    except Exception, e:
        errorString = "Can't load module %s from path %s: %s" % (moduleName, modulePath, e)
        _logger.error(errorString)
        raise Exception(errorString)

    return module

def loadObjectFromPath(objName, modulePath, required=True):
    '''
    Load a module and return a reference to a named object in that module.
    If 'required' and the object is not found, an error is raised, otherwise,
    None is returned if the object is not found.
    '''
    module = loadModuleFromPath(modulePath)
    obj    = getattr(module, objName, None)

    if obj or not required:
        return obj

    raise Exception("Module '%s' has no object named '%s'" % (modulePath, objName))

def importFrom(modname, objname):
    '''Import modname and return reference to modname.objname'''
    from importlib import import_module

    module = import_module(modname, package=None)
    return getattr(module, objname)

def importFromDotSpec(spec):
    '''
    Import object x from arbitrary dotted sequence of packages, e.g.,
    "a.b.c.x" by splitting this into "a.b.c" and "x" and calling importFrom.
    '''
    modname, objname = spec.rsplit('.', 1)
    return importFrom(modname, objname)

#
# File and directory utilities for navigating the run-time structure
#

def mkdirs(newdir):
    'Try to create the full path and ignore error if it already exists'
    from errno import EEXIST

    try:
        os.makedirs(newdir, 0777)
    except OSError, e:
        if e.errno != EEXIST:
            raise

def delete(path):
    if os.path.lexists(path):
        _logger.debug('Removing %r', path)
        os.remove(path)

def rmlink(path):
    if os.path.lexists(path) and os.path.islink(path):
        os.remove(path)

def symlink(src, dst):
    rmlink(dst)
    _logger.debug('ln -s %s %s', src, dst)
    try:
        os.symlink(src, dst)
    except Exception:
        print "Can't symlink %s to %s" % (src, dst)
        raise

def rename(direc, src, dest):
    old = os.path.join(direc, src)
    new = os.path.join(direc, dest)
    os.rename(old, new)

def filecopy(src, dst, removeDst=True):
    'Copy src file to dst, optionally removing dst first to avoid writing through symlinks'
    from shutil import copy2        # equivalent to "cp -p"

    _logger.debug("copyfile(%s,%s,%s)" % (src, dst, removeDst))
    if removeDst and os.path.islink(dst):
        os.remove(dst)

    copy2(src, dst)

def copyfiles(files, dstdir, removeDst=True):
    '''
    :param files: a list of files to copy
    :param dstdir: the directory to copy to
    :param removeDst: if True-like, remove destination file before copying
    :return: nothing
    '''
    mkdirs(dstdir)
    for f in files:
        filecopy(f, dstdir, removeDst=removeDst)

def dirFromNumber(n, prefix="", create=False):
    '''
    Compute a directory name using a 2-level directory structure that
    allows 1000 nodes at each level, accommodating up to 1 million files
    (0 to 999,999) in two levels.
    '''
    from numpy import log10     # lazy import

    config = getConfigManager()
    maxnodes = config.getParamAsInt('Core.MaxSimDirs') or 1000

    # Require a power of 10
    log = log10(maxnodes)
    if log != int(log):
        raise CoreMcsUserError("MaxSimDirs must be a power of 10 (default value is 1000)")
    log = int(log)

    level1 = n / maxnodes
    level2 = n % maxnodes

    directory = os.path.join(prefix, str(level1).zfill(log), str(level2).zfill(log))
    if create:
        mkdirs(directory)

    return directory

def getContext():
    '''
    Get the "context" from environment variables. These variables are passed to
    Runner.py via the "queue" command
    '''
    cfg = getConfigManager()
    appName = os.getenv('MCS_APP') # or cfg.getAppName()
    cfg.setAppName(appName)

    trialNum = os.getenv('MCS_TRIALNUM')
    if trialNum:
        trialNum = int(trialNum)

    simId = os.getenv('MCS_SIMID')
    if simId:
        simId = int(simId)

    expName = os.getenv('MCS_EXPNAME')
    jobNum  = getJobNum()

    return Context(simId, trialNum, expName, appName, jobNum=jobNum)

def setContext(context, appName=None, simId=None, trialNum=None, expName=None, jobNum=None):
    '''
    Set elements of a context structure, updating the environment as well.
    '''
    if appName:
        context.appName = appName
        os.environ['MCS_APP'] = appName

    if simId is not None:
        context.simId = int(simId)
        os.environ['MCS_SIMID'] = str(simId)

    if trialNum is not None:
        context.trialNum = int(trialNum)
        os.environ['MCS_TRIALNUM'] = str(trialNum)

    if expName:
        context.expName = expName
        os.environ['MCS_EXPNAME'] = expName

    if jobNum:
        context.jobNum = jobNum

def getLogDir(simId, create=False):
    '''
    :param simId: simulation id
    :param create: if True, create the directory if needed
    :return: the pathname of the log directory
    '''
    simDir = getSimDir(simId, create=create)
    logDir = os.path.join(simDir, 'log')
    if create:
        mkdirs(logDir)

    return logDir

def getSimDir(simId, create=False):
    '''
    Return and optionally create the path to the top-level simulation
    directory for the given simulation number, based on the SimsDir
    parameter specified in the config file.
    '''
    config = getConfigManager()
    simsDir = config.getParam('Core.RunSimsDir')
    if not simsDir:
        raise CoreMcsUserError("Missing required config parameter 'RunSimsDir'")

    simDir  = os.path.join(simsDir, 's%03d' % simId)  # name is of format ".../s001/"
    if create:
        mkdirs(simDir)

    return simDir

def getTrialDir(simId, trialNum, create=False):
    '''
    Return and optionally create the path to the directory for a specific
    simulation id and trial id.
    '''
    simDir = getSimDir(simId, create=False)
    trialDir = dirFromNumber(trialNum, prefix=simDir, create=create)
    return trialDir

def getExpDir(simId, trialNum, expName, create=False):
    trialDir = getTrialDir(simId, trialNum, create=create)
    expDir = os.path.join(trialDir, expName)
    if create:
        mkdirs(expDir)

    return expDir

def getExpDirFromContext(context=None, create=False):
    '''
    Return and optionally create the path to the directory for a specific experiment.
    '''
    if context is None:
        context = getContext()

    return getExpDir(context.simId, context.trialNum, context.expName, create=create)

def getCurExpDir():
    '''
    Gets the current experiment directory based on current context.
    '''
    return getExpDirFromContext(getContext())

def getCurTrialDir():
    '''
    Gets the current trial dir based on current context.
    '''
    context = getContext()
    return getTrialDir(context.simId, context.trialNum)


def randomSleep(minSleep, maxSleep):
    '''
    Sleep for a random number of seconds between minSleep and maxSleep.
    '''
    import random
    import time

    delay = minSleep + random.random() * (maxSleep - minSleep)
    _logger.debug('randomSleep: sleeping %.1f seconds', delay)
    time.sleep(delay)

#
# Support for saving and loading runtime arguments passed from the "queue"
# command to the program running on the compute node.
#
_RUNTIME_ARGS_FILENAME = 'args.json'

def saveRuntimeArgs(args, context=None):
    import json     # lazy import

    expDir = getExpDirFromContext(context=context, create=True)
    args['expName'] = context.expName
    pathname = os.path.join(expDir, _RUNTIME_ARGS_FILENAME)
    with open(pathname, 'w') as fp:
        json.dump(args, fp, indent=4)
        fp.write("\n")

def loadRuntimeArgs(dirname=None, context=None, asNamespace=False):
    '''
    Load arguments from args.json file, but retry to allow for transient
    timeout errors resulting from many jobs starting at once.
    '''
    import json     # lazy import

    if not dirname:
        dirname = getExpDirFromContext(context=context)

    pathname = os.path.join(dirname, _RUNTIME_ARGS_FILENAME)

    maxTries = 4
    minSleep = 1
    maxSleep = 4

    for i in range(maxTries):
        try:
            with open(pathname, 'r') as fp:
                args = json.load(fp)
            break

        except IOError as e:
            _logger.error("loadRuntimeArgs: %s", e)
            randomSleep(minSleep, maxSleep)

    if i == (maxTries - 1):
        raise CoreMcsSystemError("Failed to read args file after %d tries", maxTries)

    if asNamespace:
        from argparse import Namespace
        ns = Namespace()
        for key, value in args.iteritems():
            ns.__setattr__(key, value)
        args = ns

    return args

def getParamFilenames(path):
    '''
    Get the param files in a given path.
    '''
    return filter(lambda x: x.endswith(PARAM_SUFFIX), os.listdir(path))

def getClassFromConfig(cfgVar, prefix='', default=None):
    '''
    Return a class specified in the config file for the given variable,
    optionally prefixed as specified. User can pass a default class to
    be returned if the configuration file parameter is not defined.
    '''
    cfg = getConfigManager()
    configStr = prefix + cfgVar
    moduleStr = cfg.getParam(configStr)
    return parseModuleStr(moduleStr) if moduleStr else default

# def evaluate(expr, namespace):
#     '''
#     Compile a string representation of a Python code fragment and
#     evaluate it in the given namespace. Used to invoke Python funcs
#     named in parameter files.
#     '''
#     code = compile(expr, '<string>', 'eval')
#     return eval(code, namespace)

def parseModuleStr(moduleString):
    '''
    Coverts a string representation of a module/class into the actual class.
    Example: parseModuleStr('coremcs.Parameter.MatrixParameter') returns the
    class MatrixParameter.
    '''
    package, classname = moduleString.rsplit('.', 1)
    mod = __import__(package, fromlist=[''])
    return getattr(mod, classname)


def findParamData(paramList, name):
    '''
    Convenience method mostly used in testing.
    Finds a param from a given list of Parameter instances with given name.
    '''
    return filter(lambda x: x.name == name, paramList)[0].data

TRIAL_STRING_DELIMITER = ','

def parseTrialString(string):
    '''
    Converts a comma-separated list of ranges into a list of numbers.
    Ex. 1,3,4-6,2 becomes [1,3,4,5,6,2]. Duplicates are deleted.
    '''
    rangeStrs = string.split(TRIAL_STRING_DELIMITER)
    res = set()
    for rangeStr in rangeStrs:
        r = [int(x) for x in rangeStr.strip().split('-')]
        if len(r) == 2:
            r = range(r[0], r[1] + 1)
        elif len(r) != 1:
            raise ValueError('Malformed trial string.')
        res = res.union(set(r))
    return list(res)

def createTrialString(lst):
    '''
    Assemble a list of numbers into a compact list using hyphens to identify ranges.
    This reverses the operation of parseTrialString
    '''
    from itertools import groupby   # lazy import
    from operator  import itemgetter

    lst = sorted(set(lst))
    ranges = []
    for _, g in groupby(enumerate(lst), lambda (i, x): i - x):
        group = map(lambda x: str(itemgetter(1)(x)), g)
        if group[0] == group[-1]:
            ranges.append(group[0])
        else:
            ranges.append(group[0] + '-' + group[-1])
    return TRIAL_STRING_DELIMITER.join(ranges)

def chunkify(lst, chunks):
    '''
    Generator to turn a list into the number of lists given by chunks.
    In the case that len(lst) % chunksize != 0, all chunks are made as
    close to the same size as possible.
    '''
    l = len(lst)
    numWithExtraEntry = l % chunks  # this many chunks have one extra entry
    chunkSize = (l - numWithExtraEntry) / chunks + 1
    switch = numWithExtraEntry * chunkSize

    i = 0
    while i < l:
        if i == switch:
            chunkSize -= 1
        yield lst[i:i + chunkSize]
        i += chunkSize

def saveDict(d, filename):
    with open(filename, 'w') as f:
        for key, value in d.iteritems():
            f.write('%s=%s\n' % (key, value))

def deprecated(func):
    """
    This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used.
    """
    from warnings import warn

    def newFunc(*args, **kwargs):
        warn("Call to deprecated function %s." % func.__name__,
                      category=DeprecationWarning)
        return func(*args, **kwargs)
    newFunc.__name__ = func.__name__
    newFunc.__doc__ = func.__doc__
    newFunc.__dict__.update(func.__dict__)
    return newFunc

def fullClassname(obj):
    module = obj.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return obj.__class__.__name__
    return module + '.' + obj.__class__.__name__

def isdebugging():
  for frame in stack():
    if frame[1].endswith("pydevd.py"):
      return True
  return False


if __name__ == "__main__":
    filename = '/Users/rjp/tmp/final-energy-by-fuel-Reference.csv'

    truncateBigFile(filename, 100000, linesToSave=10, delete=False)
    truncateBigFile(filename, 10000,  linesToSave=50, delete=True)
