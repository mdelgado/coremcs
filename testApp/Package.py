#!/usr/bin/env python
'''
Created on Dec 9, 2013

@author: Richard Plevin

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import logging
from coremcs.Package import CorePackage
from coremcs.Configuration import getConfigManager, CoreConfiguration
from coremcs.Parameter import ScalarParameterSet

_VERSION = "TestPackage v1.0"
_logger  = logging.getLogger(__name__)


class TestConfiguration(CoreConfiguration):
    '''
    Example Configuration subclass. Each package must define a configuration
    class that subclasses CoreConfiguration (or a subclass thereof) and make
    the calls herein to ensure that all package config files (defaults.cfg)
    are read in the correct order. The package must then call getConfigManager
    passing in the local configuration class, in __init__ method of the package,
    before calling super's __init__, thus establishing this class as the config
    class used thereafter.
    '''
    def __init__(self):
        super(TestConfiguration, self).__init__()
        self.readPackageDefaults(__file__)


class TestPackage(CorePackage):
    '''
    Test package to demonstrate creating a simple derivative package.
    Methods of CorePackage that subclass may choose to override and extend:
       self.setupMainParser()
       self.add_subparser(self, name, **kwargs)
       self.get_subparser(self, name)

    These core methods add subparsers for each of the subcommands.
    Define them to add features. Be sure to call super's method
    if appropriate.
       self.addCmd_AddExp()
       self.addCmd_Analyze()
       self.addCmd_Config()
       self.addCmd_DiscreteDist()
       self.addCmd_FileUtil()
       self.addCmd_GenSim()
       self.addCmd_InitDb()
       self.addCmd_Iterate()
       self.addCmd_New()
       self.addCmd_Queue()
    '''
    def __init__(self):
        # Must instantiate the local config class before calling super
        self.config = getConfigManager(TestConfiguration)
        super(TestPackage, self).__init__()

    @classmethod
    def getParameterSetClass(cls, program):
        return ScalarParameterSet

    @classmethod
    def getInitialData(cls):
        '''
        Return the data this package needs added to a newly initialized database.
        Value must be a list of pairs, where the first element is a table name,
        and the second is a list of table rows expressed as dictionaries. Subclasses
        get the list from super and add it to their own.
        '''
        superData = super(TestPackage, cls).getInitialData()

        ourData = [
            ['Program',    [{'name': 'dummy',
                             'description': 'Trivial test application'}]],
            ['Experiment', [{'expName': 'testexp',
                             'description': 'Test experiment'}]]
        ]
        return superData + ourData

    def createOutputs(self, db):
        '''
        Create "volume" output if not already found in db
        '''
        super(TestPackage, self).createOutputs(db)
        db.createOutput('volume', description="An output value for testing", program='dummy')
