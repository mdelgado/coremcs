This directory holds the testmcs Package, a simple subclass of coremcs.Package.
It is intended primarily for testing but it also provides a basis for new
applications.

Testmcs has all the components required of a Dist-MCS Package:

- TestPackage.py contains TestPackage, a subclass of CorePackage, and
  TestConfiguration, a subclass of CoreConfiguration.

- The "template" directory holds the files to be copied into the "run"
  and "app" directories for a new User Application created from this
  package.

- The file template/run/_appName_/bin/testApp.py defines a simple
  User Program that can be run locally (using the -l or --local option
  on the "queue" sub-command) or distributed on a cluster.

- The file cfg/defaults.cfg defines Package-specific configuration
  parameters.

Finally, a shell script, "runTestApp.sh" provides a runnable example of
the command sequence command to create and run the application.
