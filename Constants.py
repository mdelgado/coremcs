'''
Created on Dec 19, 2012


@author: Richard Plevin
@author: Sam Fendell

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import re

# Status codes for invoked programs
RUNNER_SUCCESS = 0
RUNNER_FAILURE = -1

# DiscreteDist
DISCRETE_SUFFIX = 'ddist'
DEFAULT_DISCRETE_PRECISION = 100
DEFAULT_DISCRETE_TOLERANCE = 0.01

# DistGenerate
DEFAULT_BINS     = 30
DEFAULT_TRUNCATE = 3
COUNT_TITLE      = 'count'

# Distro
SPEC_SEPARATOR  = r'\s+'
DIST_TYPE_REGEX = re.compile(r'^(.*?)(delta|factor)?$')
VALID_TARGETS   = r'(rows|cols|cells|single|none)'

DISCRETE_ENTRY_SEPARATOR = ':'
DICT_SEPARATOR = '='
DISTRO_SUFFIX  = 'distro'

# Distro modifier stuff
APPLY_MODIFIER      = '_apply'
LOWBOUND_MODIFIER   = '_lowbound'
HIGHBOUND_MODIFIER  = '_highbound'
UPDATEZERO_MODIFIER = '_updateZero'
ENTRIES_MODIFIER    = '_entries'

# SymbolicSet
SETFILE_SUFFIX = 'set'

# SymbolicSet, Distro:
DICT_REGEX = '^[^%s]+%s[^%s]+$' % (DICT_SEPARATOR, DICT_SEPARATOR, DICT_SEPARATOR)

# Multiple users
COMMENT_CHAR = '#'
PARAM_SUFFIX = 'param'
