'''
Created on Jan 4, 2013

@author: Rich Plevin
@author: Sam Fendell

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import sys
import os
import shutil
import re
import argparse
import platform
import shlex
from . import log
from . import util as U
from .error import CoreMcsUserError, CoreMcsSystemError
from .Database import getDatabase, getSession
from .Parameter import ScalarParameter, ScalarParameterSet
from .Configuration import getConfigManager, CoreConfiguration
from .Constants import DEFAULT_BINS, DEFAULT_TRUNCATE, COUNT_TITLE, PARAM_SUFFIX, DISTRO_SUFFIX

_VERSION  = 'Core-MCS 0.5'

TEMPLATE_APP_DIR = 'app'
TEMPLATE_RUN_DIR = 'run'

# Command name constants
NEWAPP_CMD   = 'new'
INITDB_CMD   = 'initdb'
GENSIM_CMD   = 'gensim'
QUEUE_CMD    = 'queue'
ANALYZE_CMD  = 'analyze'
FILEUTIL_CMD = 'fileutil'
ITERATE_CMD  = 'iterate'
DISCRETE_CMD = 'discrete'
CONFIG_CMD   = 'config'
ADDEXP_CMD   = 'addexp'


_logger = log.getLogger(__name__)

def baseTrialUtil(appName, simId, trialNum, expName, trialDir, expDir, mvTrial=None, mvExp=None, symlink=None):
    if mvTrial:
        U.rename(trialDir, mvTrial[0], mvTrial[1])

    if mvExp:
        U.rename(expDir, mvExp[0], mvExp[1])

    if symlink:
        src = symlink[0]
        dst = os.path.join(expDir, os.path.basename(src))
        symlink(src, dst)


def trialIterator(args):
    """
    Run a command for each trialDir or expDir, using str.format to pass required args.
    Possible format args are: appName, simId, trialNum, expName, simDir, trialDir, and expDir.
    """
    from subprocess import call

    simId   = args.simId
    command = args.command
    expName = args.expName
    noRun   = args.noRun
    trialStr = args.trials

    cfg     = getConfigManager()
    appName = cfg.getAppName()

    if trialStr:
        trials = U.parseTrialString(trialStr)
    else:
        db = getDatabase()
        count = db.getTrialCount(simId)
        trials = xrange(count)

    context = U.Context(simId, 0, expName, appName)
    _logger.info('Running iterator for appName=%s, simId=%d, expName=%s, trials=%s, command="%s"',
                 appName, simId, expName, trialStr, command)

    # Create a dict to pass to str.format. These are constant across trials.
    argDict = {
        'appName' : appName,
        'simId'   : args.simId,
        'expName' : args.expName,
    }

    for trialNum in trials:
        argDict['trialNum'] = context.trialNum = trialNum
        argDict['trialDir'] = U.getTrialDir(simId, trialNum)
        argDict['simDir']   = U.getSimDir(simId)
        argDict['expDir']   = U.getExpDirFromContext(context, create=True)

        try:
            cmd = command.format(**argDict)
        except Exception as e:
            raise CoreMcsUserError("Bad command format: %s" % e)

        if noRun:
            print cmd
        else:
            try:
                call(cmd, shell=True)

            except Exception as e:
                raise CoreMcsUserError("Failed to run command '%s': %s" % (cmd, e))


def fileUtil(simId, expName, _trialUtil=baseTrialUtil, **kwargs):
    db      = getDatabase()
    trials  = db.getTrialCount(simId)
    cfg     = getConfigManager()
    appName = cfg.getAppName()
    context = U.Context(simId, 0, expName, appName)
    _logger.info('Running fileUtil for appName=%s, simId=%d, expName=%s, trials=%d',
                 appName, simId, expName, trials)

    for trialNum in xrange(trials):
        context.trialNum = trialNum
        expDir   = U.getExpDirFromContext(context, create=True)
        trialDir = U.getTrialDir(simId, trialNum)
        _trialUtil(appName, simId, trialNum, expName, trialDir, expDir, **kwargs)


class CorePackage(object):

    def __init__(self):
        '''
        Subclasses of CorePackage must call getConfigManager with the
        CoreConfiguration subclass defined within that package.
        '''
        self.config = getConfigManager(CoreConfiguration)

        self._parser = None
        self._mainargs = None
        self._subparsers = None
        self._parserdict = {}
        self._mainarg_re = re.compile('main_.*')    # compile this for faster matching
        self.setupMainParser()

        # Add subparsers for each of the subcommands
        self.addCmd_Analyze()
        self.addCmd_Config()
        #self._addDistGenerate()
        self.addCmd_FileUtil()
        self.addCmd_Iterate()
        self.addCmd_GenSim()
        self.addCmd_InitDb()
        self.addCmd_NewApp()
        self.addCmd_AddExp()
        self.addCmd_Queue()

    # Provide a method to allow subclass to override or wrap.
    def genDistros(self):
        from Distro import genDistros       # lazy import
        genDistros()

    # Maybe better to store the flags and destinations of all primary parser
    # arguments to ensure that a subparser doesn't override either? This could
    # be checked in add_subparser()
    def _is_main_arg(self, arg):
        return arg in self._mainargs and re.match(self._mainarg_re, arg)


    def setupMainParser(self):
        self._parser = argparse.ArgumentParser()
        parser = self._parser

        # Note that the "main_" prefix is significant; see _is_main_arg() above
        # parser.add_argument('-V', '--main_verbose', action='store_true', default=False,
        #                     help='Causes log messages to be printed to console.')

        parser.add_argument('-L', '--main_log_level', default=None,
                            choices=['notset', 'debug', 'info', 'warning', 'error', 'critical'],
                            help='Sets the log level of the program.')

        parser.add_argument('-A', '--appName', default=None,
                            help='Name of user application name. Defaults to the value of config parameter DefaultApp.')

        parser.add_argument('--version', action='version', version=_VERSION)

        self._mainargs = [x.dest for x in self._parser._actions]
        self._subparsers = self._parser.add_subparsers(dest='subcommand', title='Subcommands',
                                                       description='For help on subcommands, use the "-h" flag after the subcommand name')


    def add_subparser(self, name, **kwargs):
        '''
        Adds a new subparser with given name and kwargs to the main parsing
        routine, and returns it. Every subparser must call set_defaults to set
        a value for the parameter _func, the value of which is a function that
        takes a Namespace object and performs the desired function with it.
        '''
        parser = self._subparsers.add_parser(name, **kwargs)
        self._parserdict[name] = parser
        return parser

    def get_subparser(self, name):
        '''
        Returns the subparser for the given command. This
        allows extra options to be added in later layers of the
        application.
        '''
        return self._parserdict[name]


    def analyzeCmd(self, args):
        'Defined as an internal function to gain access to the parser variable'
        if not (args.exportFile or args.resultFile or args.plot or args.importance or
                args.groups or args.plotInputs or args.stats or args.convergence or
                args.exportEMA):
            msg = 'Must specify at least one of: --export, --resultFile, --plot, --importance, --groups, --distros, --stats, --convergence, --exportEMA'
            raise CoreMcsUserError(msg)

        from .Analysis import analyzeSimulation      # lazy import
        analyzeSimulation(args)


    def addCmd_Analyze(self):
        parser = self.add_subparser(ANALYZE_CMD,
                                    help='''Analyze simulation results stored in the database for the given simulation.
                                    At least one of -c, -d, -i, -g, -p, -t (or the longname equivalent) must be specified.''')

        parser.set_defaults(_func=self.analyzeCmd)

        # Required argument
        parser.add_argument('-s', '--simId', type=int, required=True,
                            help='The id of the simulation')

        # Optional arguments
        parser.add_argument('-c', '--convergence', action='store_true', default=False,
                            help='Generate convergence plots for mean, std dev, skewness, and 95%% coverage interval.')

        parser.add_argument('-d', '--distros', dest='plotInputs', action='store_true', default=False,
                            help='Plot frequency distributions for input parameters.')

        parser.add_argument('-e', '--expName', type=str,
                            help='The name of the experiment or scenario to run.')

        parser.add_argument('--exportEMA', type=str, default=None,
                            help='''Export results to the given .tar.gz file in a format suitable for analysis
                            using the EMA Workbench. The -e (--expName) and -r (--resultName) flags can hold
                            comma-delimited lists of experiments and results, respectively.''')

        parser.add_argument('-g', '--groups', action='store_true',
                            help='Show the uncertainty importance for groups of parameters.')

        parser.add_argument('-i', '--importance', action='store_true', default=False,
                            help='Show the uncertainty importance for each parameter.')

        parser.add_argument('-l', '--limit', type=int, required=False, default= -1,
                            help='Limit the analysis to the given number of results')

        parser.add_argument('-o', '--exportFile', type=str, default=None,
                            help='A file into which to export data for the named model result.')

        parser.add_argument('-O', '--resultFile', type=str, default=None,
                            help='''Export all model results to the given file. When used with this option,
                            the -r (--resultName) flag can be a comma-delimited list of result names.''')

        parser.add_argument('-p', '--plot', action='store_true', default=False,
                            help='''Plot a histogram of the frequency distribution for the named model output
                            (-r required).''')

        parser.add_argument('-r', '--resultName', type=str, default=None,
                            help='The name of the result variable to analyze.')

        parser.add_argument('-S', '--stats', action='store_true', default=False,
                            help='Print mean, median, max, min, std dev, skewness, and 95%% coverage interval.')

        parser.add_argument('-x', '--xlabel', dest='xlabel', type=str, default=r'g CO$_2$e MJ$^{-1}$',
                            help='Specify a label for the x-axis in the histogram.')

        return parser


    def addCmd_Config(self):
        'Add a command to set/get/list config file parameters'

        def _setConfigParams(args):
            set = 1 if args.set  else 0
            get = 1 if args.get  else 0
            lst = 1 if args.list else 0
            flags = set + get + lst

            if flags != 1:
                parser.error('Must specify exactly one of --set, --get, or --list')
                return

            sectionName = 'DEFAULT' if args.useDefault else args.appName

            if args.set:      # coremcs config --set LogLevel=WARN
                pair = args.set.split('=')
                if len(pair) != 2:
                    raise CoreMcsUserError("--set argument must be of the form PARAMETER=VALUE (with no spaces around the '=')")

                option, value = pair
                self.config.setParam(option, value, sectionName)

            elif args.get:    # coremcs config --get LogLevel
                value = self.config.getParam(args.get, sectionName)
                if value is not None:
                    print value

            elif args.list:    # coremcs config --list
                self.config.listSection(sectionName)

            else:
                print "Unknown or missing sub-command to 'config'"

        parser = self.add_subparser(CONFIG_CMD, help='Set the values of configuration parameters to override defaults.')
        parser.set_defaults(_func=_setConfigParams)

        parser.add_argument('-D', '--useDefault', default=False, action='store_true',
                            help='''Used with --set, --get, or --list to have those commands operate on the DEFAULT
                                    section rather than the section for the current default app.''')
        parser.add_argument('-s', '--set', type=str,
                            help='''Set a configuration parameter. Argument must be of the form variable=value, with no spaces.
                                    For example, to set variable "myvar" to "53", the command would be "mcs config -s myvar=53".''')
        parser.add_argument('-g', '--get', type=str,
                            help='Show the value of a default configuration parameter. Argument is a variable name.')
        parser.add_argument('-l', '--list', default=False, action='store_true',
                            help='List all configuration parameters for a section.')
        return parser


    def addCmd_DiscreteDist(self):
        def _discreteDist(args):
            from .DistGenerate import csvToDiscreteDistro
            csvToDiscreteDistro(**vars(args))

        parser = self.add_subparser(DISCRETE_CMD, help='Convert csv files to the .ddist format.')
        parser.set_defaults(_func=_discreteDist)

        parser.add_argument('-i', '--inputFile', required=True,
                            help='Path to input .csv file being converted.')

        parser.add_argument('-o', '--outputFile', required=True,
                            help='Path to output .ddist file.')

        parser.add_argument('-d', '--dataTitle', required=True,
                            help='Actual data title in the .csv file.')

        parser.add_argument('-b', '--bins', type=int, default=DEFAULT_BINS,
                            help='Number of bins to separate discrete distro into')

        parser.add_argument('-t', '--truncate', type=int, default=DEFAULT_TRUNCATE,
                            help='Number of digits to truncate output to. Default is 3')

        parser.add_argument('-c', '--countTitle', type=str, default=COUNT_TITLE,
                            help='Title of column representing counts of data.')

        parser.add_argument('-n', '--varName', type=str, default=None,
                            help='Title of rows of output distribution')

        parser.add_argument('-v', '--varTitles', type=str, nargs='*',
                            help='Titles of columns keying different distributions in the input file')
        return parser


    def addCmd_FileUtil(self):
        parser = self.add_subparser(FILEUTIL_CMD, help='''
        Perform various operations within a simulation directory:
        Create or delete links to a file in all experiment or trial directories,
        or move (rename) files in each trial or experiment directory.
        ''')
        parser.set_defaults(_func=lambda args: fileUtil(**vars(args)))

        # Required arguments
        parser.add_argument('-s', '--simId',   type=int, required=True, help='The id of the simulation')
        parser.add_argument('-e', '--expName', type=str, required=True, help='The name of the experiment')

        # Optional Arguments
        parser.add_argument('--mvTrial', nargs=2, help='Rename file arg1 to arg2 in each trial dir.')
        parser.add_argument('--mvExp',   nargs=2, help='Rename file arg1 to arg2 in each experiment dir.')
        parser.add_argument('--symlink', nargs=1, help='Create a symlink in each experiment dir the given file.')

        return parser

    def addCmd_Iterate(self):
        parser = self.add_subparser(ITERATE_CMD, help='''
        Run a command in each trialDir, or if expName is given, in each expDir.
        The following arguments are available for use in the command string,
        specified within curly braces: appName, simId, trialNum, expName, trialDir, expDir.
        For example, to run the fictional program "foo" in each trialDir for a given set of
        parameters, you might write:
        gcammcs iterate -s1 -c "foo -s{simId} -t{trialNum} -i{trialDir}/x -o{trialDir}/y/z.txt".
        ''')
        parser.set_defaults(_func=trialIterator)

        # Required arguments
        parser.add_argument('-s', '--simId',   type=int, required=True,
                            help='The id of the simulation')

        parser.add_argument('-c', '--command', type=str, required=True,
                            help='''A command string to execute for each trial. The following
                            arguments are available for use in the command string, specified
                            within curly braces: appName, simId, trialNum, expName, trialDir, expDir.''')

        parser.add_argument('-e', '--expName', type=str, default="",
                            help='The name of the experiment')

        parser.add_argument('-n', '--noRun', action='store_true',
                            help="Show the commands that would be executed, but don't run them")

        parser.add_argument('-t', '--trials', type=str, default=None,
                             help='''Comma separated list of trial or ranges of trials to run. Ex: 1,4,6-10,3.
                             Defaults to running all trials for the given simulation.''')
        return parser

    def _addFileArguments(self, parser):
        '''
        Split out from addCmd_GenSim to allow subclasses to use different
        parameter and distro file conventions.
        :param parser: the subparser for the 'gensim' command
        :return: nothing
        '''
        parser.add_argument('-d', '--distro', dest='distroFile', type=str, default=None,
                            help='Specify a file containing paramater distribution definitions. Default is {program}.%s' % DISTRO_SUFFIX)

        parser.add_argument('-p', '--param',  dest='paramFile', type=str, default=None,
                            help='Specify a file containing parameter definitions. Default is {program}.%s' % PARAM_SUFFIX)


    def addCmd_GenSim(self):
        parser = self.add_subparser(GENSIM_CMD, help='Generate input files for simulations.')
        parser.set_defaults(_func=self.genSimCmd)

        parser.add_argument('-t', '--trials', type=int, required=True,
                    help='The number of trials to create for this simulation (REQUIRED)')

        parser.add_argument('-s', '--simId', type=int,
                    help='The id of the simulation. If not provided, a new sim is created.')

        parser.add_argument('-D', '--desc', type=str, default='',
                    help='A brief (<= 256 char) description the simulation.')

        parser.add_argument('-P', '--programs', type=str, default=None,
                    help='Specify programs for which trial data should be generated. Default is to generate for all defined programs for this app.')

        self._addFileArguments(parser)      # subclass can override this

        return parser


    def addCmd_InitDb(self):
        parser = self.add_subparser(INITDB_CMD, help='''
            Initialize the database for the given user application. This is done automatically
            by the sub-command "new" and should be used only to recreate the database from scratch.''')
        parser.set_defaults(_func=self.initdbCmd)

        parser.add_argument('-r', '--deleteSims', action='store_true', default=False,
                            help='Delete all simulations from the run directory.')

        parser.add_argument('-e', '--empty', action='store_true', default=False,
                            help='''Create the database schema but don't add any data.
                            Useful when restoring from a dumped database.''')
        return parser


    def addCmd_NewApp(self):
        parser = self.add_subparser(NEWAPP_CMD, help='''
            Initialize the directory structure for a user application. Use the global -A flag to set
            the name of the new application, e.g., ./mcs -A myApp new --appRoot ~/sample/app -r ~/run''')
        parser.set_defaults(_func=self.newAppCmd)

        # Optional args. Note that -A (--appName) flag is required with the "new" sub-command.
        parser.add_argument('-a', '--appRoot', default=None,
                            help='''Directory in which to store user application directories. Defaults to
                            value of config parameter Core.AppRoot''')

        parser.add_argument('-r', '--runRoot', default=None,
                            help='''Root of the run-time directory for running user programs. Defaults to
                            value of config parameter Core.RunRoot''')

        parser.add_argument('-s', '--setAppName', action='store_true',
                            help='''Store the given appName in the user's config file as the default
                            for subsequent operations.''')
        return parser


    def addCmd_AddExp(self):
        parser = self.add_subparser(ADDEXP_CMD, help='Adds the named experiment to the database, with an optional description.')
        parser.set_defaults(_func=self.addexpCmd)

        parser.add_argument('-e', '--expName', type=str, required=True,
                            help='Add the named experiment to the database.')

        parser.add_argument('-d', '--description', type=str, required=False, default='No description',
                            help='Add the named experiment to the database.')


    def addCmd_Queue(self):
        parser = self.add_subparser(QUEUE_CMD, help='Queues the given number of jobs to run on compute nodes.')
        parser.set_defaults(_func=self.queueCmd)

        parser.add_argument('-s', '--simId', type=int, required=True,
                            help='The id of the simulation (required)')

        cfg = self.config
        batchSystem  = cfg.getParam('Core.BatchSystem')
        batchPrefix  = batchSystem + '.'
        otherArgsOpt = batchPrefix + 'OtherArgs'
        queueNameOpt = batchPrefix + 'QueueName'
        enviroVars   = batchPrefix + 'EnviroVars'

        defaultExp = cfg.getParam('Core.DefaultExpName', section='DEFAULT')
        expHelp    = ' Default value is "%s".' % defaultExp if defaultExp else ""

        parser.add_argument('-e', '--experiments', dest='expList', type=str,
                            # experiment is required if no default is set; otherwise use default if not specified
                            required=(not defaultExp), default=defaultExp,
                            help='The name of the experiment. May be a comma-separated list of names.' + expHelp)

        parser.add_argument('-E', '--noEpilogue', action='store_true',
                            help='If set, the epilogue script will not be run. (PBS only)')

        # Default on Mac is to run locally
        PlatformName = platform.system()
        runLocalDefault = (PlatformName == 'Darwin' or cfg.getParamAsBoolean('Core.RunLocal'))
        parser.add_argument('-l', '--runLocal', action='store_true', default=runLocalDefault,
                            help='''Runs the program locally instead of submitting it as a batch job. (It's
                            not necessary to specify this flag on OS X since only local execution is supported.)''')

        parser.add_argument('-m', '--minutes', dest='minPerTask', type=float, default=cfg.getParamAsFloat('Core.MinPerTask'),
                            help='''Set the number of minutes to allocate for each job submitted.
                            Overrides config parameter Core.MinPerTask.''')

        parser.add_argument('-o', '--otherArgs', default=cfg.getParam(otherArgsOpt),
                            help='''Any additional command line arguments to pass through to the batch command.
                            Defaults to value of config parameter %s.''' % otherArgsOpt)

        # If alternative flags are added, the hack in run() must be updated
        parser.add_argument('--programArgs', type=str, default=cfg.getParam('Core.ProgramArgs'),
                            help='''Arguments to pass to user program. Quote sequences that include spaces, e.g.,
                                    to pass args: -x foo, use --programArgs="-x foo"''')

        parser.add_argument('-q', '--queueName', type=str, default=cfg.getParam(queueNameOpt),
                            help='''The queue or partition to submit jobs to. Overrides config file
                            parameter %s.''' % queueNameOpt)

        parser.add_argument('-Q', '--queueLimit', type=int, default=cfg.getParamAsInt('Core.QueueLimit'),
                            help='''Set maximum number of jobs to submit to the queue.
                            Overrides config parameter Core.QueueLimit.''')

        parser.add_argument('-r', '--redo', choices=['failed', 'killed', 'queued', 'aborted', 'alarmed', 'running'],
                            action='append', default=[],
                            help='''Re-launch all trials for the given simId with the status specified.
                            Multiple -r / --redo flags may be specified to indicate more than one status.''')

        parser.add_argument('-R', '--redoListOnly', action='store_true', default=False,
                            help='Used with -r to only list the trials to redo, then quit.')

        parser.add_argument('-t', '--trials', type=str, default='',
                             help='''Comma separated list of trials or ranges of trials to run. Ex: 1,4,6-10,3.
                             Defaults to running them all.''')

        parser.add_argument('-v', '--enviroVars', type=str, default=cfg.getParam(enviroVars),
                            help='''Comma-delimited list of environment variable assignments to pass to batch job,
                            e.g., "FOO=1,BAR=2". Combined with the batch-system-specific command-line flag.''')

        return parser


    def copySimulationFiles(self, appDataDir, simDataDir):
        '''
        Copy files required by this simulation to the run directory.
        '''
        # Ensure that DataDir isn't a parent of simDir.
        prefix = os.path.commonprefix( [appDataDir, os.path.normpath(simDataDir)] )
        if prefix == appDataDir:
            raise CoreMcsUserError("Can't copytree '%s' into '%s'" % (appDataDir, simDataDir))

        _logger.info("copytree(%s, %s)" % (appDataDir, simDataDir))
        shutil.copytree(appDataDir, simDataDir)   # TODO: use symlinks=True?


    def afterGenTrial(self, simId, trialNum, trialDir, paramFile):
        '''
        Hook for subclasses to take some action after the data for one trial
        have been written to the trialDir. Default is to do nothing.
        '''
        pass

    @classmethod
    def defineSets(cls):
        '''
        Read set files, if any. Subclasses can use this hook to define sets or read in set files
        '''
        pass

    @classmethod
    def getDistroFile(cls, program):
        '''
        Given a program name, compute the name of the corresponding distribution file.
        Subclasses can override this to return whatever they like.
        '''
        return program + '.' + DISTRO_SUFFIX


    @classmethod
    def getParamFile(cls, program):
        '''
        Given a program name, compute the name of the corresponding parameter file.
        Subclasses can override this to return whatever they like.
        '''
        return program + '.' + PARAM_SUFFIX

    @classmethod
    def getParameterClass(cls, program):
        return ScalarParameter

    @classmethod
    def getParameterSetClass(cls, program):
        return ScalarParameterSet

    def getClassFromConfig(self, cfgVar, program='', default=None):
        '''
        The config variable name is constructed by concatenating prefix
        and cfgVar. Note that a "." is NOT inserted. If not found, the
        value of 'default' is returned.
         '''
        cls = U.getClassFromConfig(cfgVar, prefix=program, default=default)
        return cls

    def genSimulation(self, program, simId, trials, paramFile, distroFile, args):
        '''
        Generic method for generating a parameter file based on the distributions in the given distroFile.
        Subclasses can override all the critical elements to customize this, per program, as needed.
        '''
        # from .Parameter import MatrixParameter     # lazy imports to improve startup time
        # from .ParameterSet import AbsBaseParameterSet

        cfg = self.config
        appProgDir = cfg.getParam('Core.AppProgDir')

        distroPath = os.path.join(appProgDir, program, distroFile)
        paramsPath = os.path.join(appProgDir, program, paramFile)

        # TBD: this is too rigid; allow for other numbers/types of files
        if not os.path.exists(distroPath):
            raise CoreMcsUserError('Distribution file "%s" does not exist' % distroPath)

        if not os.path.exists(paramsPath):
            raise CoreMcsUserError('Parameter file "%s" does not exist' % paramsPath)

        _logger.info("Generating %d trials for program '%s' using param and distro files:\n  %s\n  %s",
                     trials, program, paramsPath, distroPath)

        # Package subclasses must define this class method
        paramSetClass = self.getParameterSetClass(program)
        pset = paramSetClass(program, paramsPath, distroPath)
        pset.genTrials(trials)

        # "cp -p" the parameter and distro files to simDir for reference
        simDir = U.getSimDir(simId, create=True)
        shutil.copy2(paramsPath, simDir)
        shutil.copy2(distroPath, simDir)

        paramClass = pset.parameterClass()

        for trialNum in xrange(trials):
            params    = pset.getTrialData(simId, trialNum)
            trialDir  = U.getTrialDir(simId, trialNum, create=True)
            filename = os.path.join(trialDir, paramFile)
            _logger.debug('Writing %s' % filename)
            paramClass.writeFile(filename, params)

        _logger.info("Generated %s trials for program %s, simId %s." % (trials, program, simId))

    def genSimCmd(self, args):
        '''
        Generate a simulation. Do generic setup here, then call genSimulation(), which
        can be more easily overridden in Package subclasses. Returns the simId.
        '''
        cfg = self.config
        simId   = args.simId
        trials  = args.trials
        desc    = args.desc

        # The simId can be provided on command line, in which case we need
        # to delete existing parameter entries for this app and simId.
        db     = getDatabase()
        simId  = db.createSim(trials, desc, simId=simId)

        # Generate DistroGen objects. Subclasses can extend this if needed.
        self.genDistros()

        programs = cfg.getUserPrograms()

        if not programs:
            raise CoreMcsSystemError("No Programs defined in config file")

        # Allow user to specify a comma-separated list of programs to gen sim data for,
        # skipping unnamed programs.
        if args.programs:
            requestedPrograms = set(args.programs.split(','))
            if not requestedPrograms.issubset(set(programs)):
                raise CoreMcsUserError("The specified programs (%s) are not all defined in the config file" % args.programs)

            programs = requestedPrograms    # replace full list with user-specified programs

        # loop over programs and generate simulation data for each
        for program in programs:
            # base implementation expects two files: {program}.distro and {program}.params
            # if subclass overrides _addFileArguments, one or both of these may not exist
            distroFile = None if 'distroFile' not in args else (args.distroFile or self.getDistroFile(program))
            paramFile  = None if 'paramFile'  not in args else (args.paramFile  or self.getParamFile(program))
            self.genSimulation(program, simId, trials, paramFile, distroFile, args=args)

        return simId

    def queueBatchJob(self, appName, simId, trials, jobNum, expName, cfgSpec,
                      batchArgs=None, batchCommand=None, runLocal=False,
                      logDir=None, enviroVars=None, enviroVarsFlag=''):
        '''
        Calculate parameters to the batch queueing command and execute the command.
        '''
        import subprocess

        os.chdir(logDir)    # so log files are created here

        trialStr = U.createTrialString(trials)
        path     = os.path.split(__file__)[0]
        program  = os.path.join(path, 'Runner.py')

        environ = [('MCS_APP',     appName),
                   ('MCS_SIMID',   str(simId)),
                   ('MCS_TRIALS',  trialStr),
                   ('MCS_EXPNAME', expName),
                   ('MCS_CFG',     cfgSpec)]

        # We need these both as strings for batch and as dict for exporting to environment
        if enviroVars:
            pairs = enviroVars.split(',')
            environ += map(lambda s: s.split('='), pairs)

        if runLocal:
            from . import Runner   # lazy import

            # Export all vars to environment
            for name, value in environ:
                os.environ[name] = value

            _logger.debug("Running %s locally, trials: %s, experiment: %s " % (program, trialStr, expName))
            return Runner.main(runLocal=True)

        # Create environment variable option using batch-system-specific flag
        pairs = ["%s=%s" % (name, value) for name, value in environ]
        enviroVarsOpt = enviroVarsFlag + ','.join(pairs)

        logPath, logFile, jobName = U.computeLogPath(simId, expName, logDir, trials)
        batchArgs['logPath'] = logPath    # full path required on PBS, but not slurm
        batchArgs['logFile'] = logFile
        batchArgs['jobName'] = jobName
        batchArgs['enviroVars'] = enviroVarsOpt

        try:
            command = batchCommand.format(**batchArgs)
        except KeyError as e:
            raise CoreMcsUserError('''Badly formatted batch command string in config file: %s.\n
                                      Valid keywords are %s.''' % (e, batchArgs.keys()))
        cmdArgs = shlex.split(command)
        cmdArgs.append(program)

        cmd = " ".join(cmdArgs)

        jobStr = subprocess.check_output(cmdArgs, shell=False)

        result = re.search('\d+', jobStr)
        jobId  = int(result.group(0)) if result else 999999

        _logger.info("[%04d, %8d] %s" % (jobNum + 1, jobId, cmd))

        return jobId


    def saveRuntimeArgs(self, trialNum, expName, **kwargs):
        '''
        Save runtime arguments in a file so they can be passed to the de-queued program.
        '''
        appName = kwargs['appName']
        simId   = kwargs['simId']
        context = U.Context(simId, trialNum, expName, appName)
        U.saveRuntimeArgs(kwargs, context=context)

    def queueCmd(self, args):
        '''
        Calls the batch job queuing program the required number of times to queue up
        the user app to run on each compute core. Runs are batched together (i.e.,
        they're run serially) if the number of trials exceeds the 'QueueLimit'
        config parameter.
        '''
        cfg = self.config

        # note that all cmd-line args have appropriate defaults
        simId          = args.simId
        expList        = args.expList.split(',')
        appName        = args.appName
        trials         = args.trials      # string representing a list of trials, e.g., "1,3-4,10-12"
        minPerTask     = args.minPerTask
        queueLimit     = args.queueLimit
        queueName      = args.queueName
        runLocal       = args.runLocal
        otherArgs      = args.otherArgs
        enviroVars     = args.enviroVars
        noEpilogue     = args.noEpilogue

        if runLocal:
            _logger.info('running locally')

        db = getDatabase()

        redoList = []
        for status in args.redo:
            redoList += db.getRunsWithStatus(simId, expList, status)

        # If any of the "redo" options find trials, use redoList instead of trials
        if args.redo:
            if not redoList:
                _logger.warn('No trials to re-run for simId %d with status in %s', simId, args.redo)
                return

            trials = U.createTrialString(redoList)

            if args.redoListOnly:
                print "Trials to redo: %s" % trials
                return

        count = db.getTrialCount(simId)
        if not trials:              # if trials isn't specified, queue all of them
            trials = range(count)
        else:
            trials = U.parseTrialString(trials)     # convert string like "4,7,9-12,42" to a list of the implied numbers
            maxTrial = max(trials)
            if maxTrial >= count:
                e = CoreMcsUserError('Trial number %s out of range: %s trials defined' % (maxTrial, count))
                _logger.error(e.message)
                raise e
            count = len(trials)

        # If more trials than queueLimit, each "job" will run multiple trials
        queueLimit   = sys.maxint if queueLimit < 1 else queueLimit
        jobsToSubmit = min(count * len(expList), queueLimit)

        session = getSession()

        # Log directory is within the sim directory to avoid having all
        # sims clog up a single directory with many thousands of files.
        logDir = U.getLogDir(simId, create=True)

        batchPrefix  = cfg.getParam('Core.BatchSystem') + '.'
        batchCommand = cfg.getParam(batchPrefix + 'BatchCommand')

        # TBD: document that EpilogueOpt should incorporate EpiloguePath
        # (They're separate to allow check for script and to allow user to override)
        epiloguePath = cfg.getParam(batchPrefix + 'EpiloguePath')
        epilogueOpt  = cfg.getParam(batchPrefix + 'EpilogueOpt')

        # User can issue cmd-line flag to suppress epilogue script
        if noEpilogue or not epiloguePath:
            epilogueOpt = None
        else:
            if not os.path.lexists(epiloguePath):
                _logger.warn('Specified epilogue script "%s" was not found', epiloguePath)
                epilogueOpt = None
            elif not os.access(epiloguePath, os.X_OK):
                _logger.warn('Specified epilogue script "%s" exists but is not executable', epiloguePath)
                epilogueOpt = None

        # Add in epilogue flag if not suppressed on the command-line
        if epilogueOpt:
            batchCommand += ' ' + epilogueOpt

        # this is just the flag itself, e.g., '-v' for PBS or '--export=' for SLURM
        enviroVarsFlag = cfg.getParam(batchPrefix + 'EnviroVarsFlag')

        # This dictionary is applied to the string value of {PBS,SLURM}.BatchCommand, via
        # the str.format method, which can specify options using any of these keys.
        batchArgs = {'walltime'  : 'set below',
                     'minPerTask': minPerTask,
                     'queue'     : queueName,
                     'partition' : queueName,   # alt name for SLURM
                     'jobName'   : 'set in queueBatchJob',
                     'logFile'   : 'set in queueBatchJob',
                     'logDir'    : cfg.getParam('Core.RunLogDir'),
                     'otherArgs' : otherArgs,
                     'enviroVars': enviroVars,
                     }

        argDict = vars(args)
        argDict['dbSpec'] = U.fullClassname(db)
        cfgSpec = U.fullClassname(cfg) # Runner.py must load this configuration class

        for jobNum, chunk in enumerate(U.chunkify(trials, jobsToSubmit)):
            argDict['trials'] = chunk

            for expName in expList:
                expId = db.getExpId(expName, session=session)
                if expId is None:
                    raise CoreMcsUserError("Unknown experiment name '%s'" % expName)

                # Add a run record for each trial in this chunk
                for trialNum in chunk:
                    # Add a record in the "run" table listing this trial as "queued"
                    # (old run for this simid, trialnum and expid is deleted if it exists)
                    newRun = db.createRun(simId, trialNum, expId=expId, session=session)

                    # Save parameters needed by Runner.py in a .json file in the expDir
                    self.saveRuntimeArgs(trialNum, expName, **argDict)

                session.commit()

                minPerSub = minPerTask * len(chunk)
                walltime   = "%02d:%02d:00" % (minPerSub / 60, minPerSub % 60)
                batchArgs['walltime'] = walltime

                # queue one job for the whole chunk
                jobId = self.queueBatchJob(appName, simId, chunk, jobNum, expName, cfgSpec,
                                           batchArgs=batchArgs, batchCommand=batchCommand,
                                           runLocal=runLocal, logDir=logDir,
                                           enviroVars=enviroVars, enviroVarsFlag=enviroVarsFlag)
        session.close()

    def createOutputs(self, db):
        '''
        Simple hook to allow subclass Packages to add model outputs by calling
        e.g., db.createOutput('foo', description="Test output", program='dummy')
        This is called by CoreDatabase in initDb
        '''
        pass

    # TBD: add args to allow copying (i) just app tree, (ii) just the run tree, or (iii) both.
    def newAppCmd(self, args):
        '''
        Setup the app and run directories for a given user app.
        '''
        cfg = self.config

        appName = args.appName
        if not appName:
            _logger.error("The -A or --appName flag is required for the sub-command 'new'")
            return

        templateDir = cfg.getTemplateDir()
        _logger.info('Template dir is %s' % templateDir)

        # Compute the name of the template directories
        appsTemplate = os.path.join(templateDir, TEMPLATE_APP_DIR)
        runTemplate  = os.path.join(templateDir, TEMPLATE_RUN_DIR)

        appRoot = args.appRoot or cfg.getParam('Core.AppRoot', section=appName)
        runRoot = args.runRoot or cfg.getParam('Core.RunRoot', section=appName)

        if not appRoot:
            raise CoreMcsUserError("AppRoot was not set on command line or in configuration file")

        if not runRoot:
            raise CoreMcsUserError("RunRoot was not set on command line or in configuration file")

        if args.setAppName:
            # do this only if user requests it (principle of least astonishment)
            _logger.info('Setting default AppName to "%s"' % appName)
            cfg.setAppName(appName, setDefault=True)

        cfg.setAppName(appName)
        cfg.setAppVariable(appName, 'Core.AppName', appName)    # save appName as Core.AppName in this section

        # Create the run and app directories, if needed
        U.mkdirs(runRoot)
        U.mkdirs(appRoot)

        # Copy the template directory, replacing the _appName_ with actual app name.
        def templateCopy(arg, dirname, names):
            newDirname = dirname.replace('_appName_', appName)
            pattern = '%s|%s' %(TEMPLATE_APP_DIR, TEMPLATE_RUN_DIR)
            leafDir = re.split(pattern, newDirname)[1]

            # Avoid copying .svn, .git, and other such directories.
            # TODO: generalize to avoid dotfiles
            if not leafDir or '.svn' in leafDir:
                return

            # print 'templateCopy on arg=%s,dirname=%s,names=%s' % (arg, dirname, names)
            # The leafdir has an extra / in front of it that messes up the join.
            # This might lead to portability issues...?
            firstChar = 1 if os.path.isabs(leafDir) else 0
            copyDir = os.path.join(arg, leafDir[firstChar:])

            if not os.path.exists(copyDir):
                os.mkdir(copyDir)
            for name in names:
                infilePath = os.path.join(dirname, name)
                if os.path.isfile(infilePath):
                    outfilePath = os.path.join(copyDir, name.replace('_appName_', appName))
                    shutil.copy2(infilePath, outfilePath)

        def errhandler(x):
            'Error handler callable by os.walk()'
            raise x

        def skipDotFiles(_arg, _dirname, names):
            '''
            Called by os.walk() for each directory examined. Delete all
            dot files from the list names so recursive descent ignores them
            when copying templates. (Mainly to skip .svn directories.)
            '''
            for i, name in enumerate(names):
                if name.startswith('.'):
                    del names[i]

        # print 'calling walk on template dir ', appsTemplate
        for dirpath, _, filenames in os.walk(appsTemplate, skipDotFiles, onerror=errhandler):
            #print "templateCopy(%s,%s,%s)" % (appRoot, dirpath, filenames)
            templateCopy(appRoot, dirpath, filenames)

        # print 'calling walk on template dir ', runTemplate
        for dirpath, _, filenames in os.walk(runTemplate, onerror=errhandler):
            #print "templateCopy(%s,%s,%s)" % (runRoot, dirpath, filenames)
            templateCopy(runRoot, dirpath, filenames)

        cfg.setAppDir(appName, os.path.join(appRoot, appName))
        cfg.setRunDir(appName, os.path.join(runRoot, appName))

        getDatabase()   # ensures database initialization


    def addexpCmd(self, args):
        '''
        Add the named experiment, with optional description, to the database.
        '''
        db = getDatabase()
        expId = db.createExp(args.expName, args.description)
        _logger.debug("Added experiment '%s' with id=%d" % (args.expName, expId))


    @classmethod
    def getInitialData(cls):
        '''
        Return the data this package needs added to a newly initialized database.
        Value must be a list of pairs, where the first element is a table name,
        and the second is a list of table rows expressed as dictionaries. Subclasses
        get the list from super and add it to their own.
        '''
        initialData = []
        #     ['Program',    [{'name': 'dummy',
        #                      'description': 'Trivial sample application'}]],
        #     ['Experiment', [{'expName': 'testexp',
        #                      'description': 'Test experiment'}]]
        # ]
        return initialData


    def initdbCmd(self, args):
        '''
        Set up the base coremcs database. Must call startDb() before calling this.

        TODO: Database initialization now occurs in getDatabase() if needed. This
        command is semantically more like "resetApp:, which both deletes and recreates
        the database schema and removes the simulation directory
        '''
        if args.deleteSims:
            cfg = self.config

            # Remove the whole sims dir and remake it
            runSimsDir = cfg.getParam('Core.RunSimsDir')
            if os.path.exists(runSimsDir):
                try:
                    shutil.rmtree(runSimsDir)
                    os.mkdir(runSimsDir)
                except Exception, e:
                    raise CoreMcsSystemError('Failed to recreate sim dir %s: %s' % (runSimsDir, e))

        db = getDatabase()

        # reinitialize the db
        db.initDb(args=args)

    def subcmdsThatDontStartDb(self):
        """
        Return a list of subcommands for which the database is not started. Implemented
        as a method to allow subclasses to amend the list if needed, e.g., for
        "gcammcs copyxml".
        """
        return [INITDB_CMD, NEWAPP_CMD, CONFIG_CMD]

    def run(self):
        '''
        Run the top-level package
        '''
        cfg = self.config
        log.configure(cfg)

        # Make sure none of the subparser arguments overwrite main parser arguments
        for name, parser in self._parserdict.iteritems():
            for action in parser._actions:
                dest = action.dest
                if self._is_main_arg(dest):
                    raise CoreMcsSystemError(("Action '%s' in parser '%s' overwrites action of main parser. " +
                        "Make sure its destination doesn't start with 'main_'") % (action.dest, name))

        # Hack to deal with bug in argparse. Values of arguments can't start with a dash (-), but we
        # want arbitrary strings to be stored in the programArgs option of queue subcommand, including
        # args with dashes. Alter the system args before calling parse_args() by prepending '#' to the
        # programArgs argument value, which is stripped after parse_args() does its thing.
        if '--programArgs' in sys.argv:
            ind = sys.argv.index('--programArgs')
            sys.argv[ind + 1] = '#' + sys.argv[ind + 1]

        args   = self._parser.parse_args()
        subcmd = args.subcommand.strip()

        #defaultAppNameSet = False

        if args.appName:
            cfg.setAppName(args.appName)
            log.resetLevel(cfg)         # set log level to the newly set appName's preferred value
        else:
            args.appName = cfg.getAppName()

        if subcmd == QUEUE_CMD and args.programArgs:
            args.programArgs = args.programArgs[1:]

        if args.main_log_level:
            log.setLevel(args.main_log_level)

        # if defaultAppNameSet:
        #     _logger.info('Setting default AppName to "%s"' % args.appName)

        #_logger.debug('Running Application with arguments %s' % args)

        # Get rid of the args to the main
        for main_key in self._mainargs:
            if re.match(self._mainarg_re, main_key):
                delattr(args, main_key)

        # Don't bother with the database for config commands.
        # Can't start database for "new" until the template is copied...
        subcmds = self.subcmdsThatDontStartDb()
        if subcmd not in subcmds:
            _db = getDatabase()          # calls startDb()

            # Define any sets used by this package, either by reading set files or directly calling SymSet()
            self.defineSets()

        # Subparsers must set _func to a function taking a namespace argument.
        _func = args._func

        # Remove these so the subcommand doesn't see them
        del args._func
        del args.subcommand

        # Runs the subcommand
        _func(args)


def runPackage(packageClass):
    '''
    Instantiate a package of the given class and call its run method, trapping any exceptions
    '''
    pkg = None
    status = -1

    try:
        pkg = packageClass()
        U.setRunningPackage(pkg)
        pkg.run()
        status = 0

    except CoreMcsUserError, e:
        sys.stderr.write("CoreMCS user error: %s\n" % e.message)

    except CoreMcsSystemError, e:
        sys.stderr.write("CoreMCS system error: %s\n" % e.message)

    except Exception, e:
        sys.stderr.write("Error running package %s: %s\n" % (packageClass, e))

        if pkg is None or pkg.config.getParamAsBoolean('Core.TracebackOnError'):
            import traceback    # lazy import
            _logger.error(traceback.print_exc())

    sys.exit(status)
