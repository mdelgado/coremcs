'''
Created on 11/17/14

@author: Richard Plevin

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''

class CoreMcsException(Exception):
    'Base class for application-related errors.'
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class CoreMcsUserError(CoreMcsException):
    'The user provided an incorrect parameter or failed to provide a required one.'
    pass


class CoreMcsSystemError(CoreMcsException):
    'Some application-related runtime error.'
    pass


class FileExistsError(CoreMcsSystemError):
    def __init__(self, filename):
        self.msg = "File %r already exists" % filename


class BaseSpecError(CoreMcsSystemError):
    filename = ''
    lineNum = 0

    def __init__(self, message):
        self.message = 'File %s, line %i: ' % (self.filename, self.lineNum) + message

    def __str__(self):
        return repr(self.message)


class ShellCommandError(CoreMcsSystemError):
    def __init__(self, msg, exitStatus=0):
        self.msg = msg
        self.exitStatus = exitStatus

    def __str__(self):
        statusMsg = " exit status %d" % self.exitStatus if self.exitStatus else ""
        return "ShellCommandError: %s%s" % (statusMsg, self.exitStatus)

class TimeoutError(CoreMcsException):
    pass

class AlarmError(CoreMcsException):
    pass
