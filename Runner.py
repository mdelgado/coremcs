#!/usr/bin/env python

'''
@author: Richard Plevin
@author: Sam Fendell

This module provides a wrapper around the user's program. It is is invoked
on the compute node and provides signal trapping to save the "killed" state in the
database, and generally to update the database with each jobs current run status.

If user's program is a python script (i.e., its is {ModelName}.py), it is loaded
as a module, and if function "runner" exists, it is called with the same args that
would have been used to invoke an executable file. If the function "runner" does
not exist, an error is raised.

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''

import os
import traceback
import shlex
from signal import signal, alarm, SIGTERM, SIGQUIT, SIGALRM
from subprocess import call

from coremcs import log
from coremcs import util as U
from coremcs.error import CoreMcsSystemError, CoreMcsUserError, TimeoutError, AlarmError
from coremcs.Database import getDatabase, RUN_RUNNING, RUN_SUCCEEDED, RUN_FAILED, RUN_KILLED, RUN_ABORTED, RUN_ALARMED
from coremcs.Configuration import getConfigManager
from coremcs.Constants import RUNNER_SUCCESS, RUNNER_FAILURE

_logger = log.getLogger(__name__)


# We catch only these 3 signals. Can extend this if needed.
def signame(signum):
    if signum == SIGTERM:
        return 'SIGTERM'

    if signum == SIGQUIT:
        return 'SIGQUIT'

    if signum == SIGALRM:
        return 'SIGALRM'

    return 'signal %d' % signum


def sighandler(signum, _frame):
    msg = "Process received " + signame(signum)

    if signum == SIGALRM:
        raise AlarmError(msg)
    else:
        raise TimeoutError(msg)


# Use this to set alarm if len(trials) > 1.
def run(runLocal=False):
    '''
    Run the user's program in the current directory, which must be set by the batch command
    since we find the rest of our run-time information here in the file args.json.
    '''

    if not runLocal:
        U.randomSleep(2, 15) # sleep between 1 and 15 secs so all jobs don't start at once

    # MCS_CFG must be in dotted format, e.g., 'a.b.SomeConfigManagerClass' (with any number of levels before class)
    cfgSpec  = os.environ.get('MCS_CFG')
    cfgClass = U.importFromDotSpec(cfgSpec)    # Split 'a.b.c' into ('a.b', 'c')
    cfg      = getConfigManager(cfgClass)
    log.configure(cfg)

    _logger.debug("Entered Runner.run")

    context = U.getContext()

    trialsStr = os.environ.get('MCS_TRIALS')

    if trialsStr:
        trials = U.parseTrialString(trialsStr)
        firstTrial = trials[0]
    else:
        # No trials specified => run all trials.
        firstTrial = 0

    # Load the arguments for the first trial, which for present purposes
    # are the same as for all other trials for this experiment.
    U.setContext(context, trialNum=firstTrial)
    runArgs = U.loadRuntimeArgs(context=context, asNamespace=True)

    # MCS_DB must be in dotted format, e.g., gcammcs.GcamDatabase
    dbSpec  = runArgs.dbSpec  # os.environ.get('MCS_DB')
    dbClass = U.importFromDotSpec(dbSpec)
    db      = getDatabase(dbClass=dbClass, checkInit=False)

    signal(SIGTERM, sighandler)
    signal(SIGQUIT, sighandler)
    signal(SIGALRM, sighandler)     # set alarm to prevent runaway tasks from consuming entire allocation

    if not trialsStr:
        # if not given explicitly, run all trials
        trialCount = db.getTrialCount(context.simId)
        trials = range(trialCount)          # start with trialNum 0

    progArgs = shlex.split(runArgs.programArgs)

    # If the program is a python module, load it. If runner() exists, call it directly.
    # Otherwise (if runnerFunc == None), the program is run as a separate process.
    program = cfg.getParam('Core.ExecutableFile', default=context.appName)

    # If program starts with "/", use it, otherwise treat it as relative
    # to RunBinDir.
    if not os.path.isabs(program):
        runBinDir = cfg.getParam('Core.RunBinDir')
        program = os.path.join(runBinDir, program)

    runnerFunc = None
    if program.endswith('.py') or program.endswith('.pyc'):
        runnerFunc = U.loadObjectFromPath('runner', program, required=False)

    if runnerFunc and callable(runnerFunc):
        _logger.info("Running '%s' directly in python" % program)
    else:
        _logger.debug("No 'runner' function found in %s" % program)

    # set which function to call
    runner = runnerFunc or call

    minPerTask = float(runArgs.minPerTask) # os.environ.get('MCS_MINUTES'))
    seconds    = max(30, int(minPerTask * 60) - 45)         # Hopefully we get alarm before system kills us; it's more reliably handled

    c = U.getContext()
    logDir = U.getLogDir(c.simId)
    logPath, logFile, jobName = U.computeLogPath(c.simId, c.expName, logDir, trials)

    try:
        for trialNum in trials:
            _logger.info('Running trial %d (alarm set for %d sec)' % (trialNum, seconds))
            alarm(seconds)      # don't let any job use more than its allotted time

            U.setContext(context, trialNum=trialNum)

            runDir = U.getExpDirFromContext(context, create=True)
            run    = db.getRunFromContext(context)
            db.setRunStatus(run.runId, RUN_RUNNING, jobNum=context.jobNum)

            try:
                os.chdir(runDir)
            except OSError as e:
                errorString = "Can't chdir: %s" % e
                _logger.error(errorString)
                raise

            _logger.info("runDir is %s", runDir)

            try:
                # Invoke the runner function directly or execute the program
                argv = [program] + progArgs
                exitCode = runner(argv)
                status = RUN_SUCCEEDED if exitCode == 0 else RUN_FAILED

                if runLocal and status == RUN_FAILED:
                    raise CoreMcsSystemError('Local run failed; aborting')

            except TimeoutError:
                _logger.error("Trial %d terminated by system", trialNum)
                status = RUN_KILLED

            except AlarmError:
                _logger.info("Trial %d terminated by SIGALRM", trialNum)
                status = RUN_ALARMED

            except CoreMcsUserError:
                status = RUN_FAILED     # avoid traceback for CoreMcs error

            except Exception as e:
                status = RUN_ABORTED     # run-time error in loaded module
                if cfg.getParamAsBoolean('Core.TracebackOnError'):
                    _logger.error(traceback.print_exc())
                _logger.fatal('%s: %s', program, e)

                if runLocal:
                    raise       # re-raise the error

            alarm(0)  # turn off timer
            db.setRunStatus(run.runId, status)
            _logger.info('Finished runId %i, trial %d: %s' % (run.runId, trialNum, status))

            # If killed by the system, return; if the alarm went off,
            # we should have time left for any subsequent trials.
            if status == RUN_KILLED:
                U.truncateBigFile(logPath)
                _logger.fatal('Runner killed by system; exiting')
                return RUNNER_FAILURE
    except:
        raise

    finally:
        U.truncateBigFile(logPath)

    # runner can succeed even if runs fail; it "fails" only if killed by system
    return RUNNER_SUCCESS


def main(runLocal=False):
    runStatus = run(runLocal=runLocal)
    _logger.debug('Exiting Runner.py with status %s', runStatus)
    return runStatus


#
# Call main program
#
if __name__ == "__main__":
    main()
