'''
Refactored Nov 23, 2015 from original which was created on Jan 13, 2013

@author: Rich Plevin

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import copy
from collections import OrderedDict
import numpy as np

from . import log
from .Constants import HIGHBOUND_MODIFIER, LOWBOUND_MODIFIER
from .Database import Input, InValue
from .util import is_number, fileReader
from .Distro import makeDistroKey
from .CorrelationDef import CorrelationPair
from .Parameter import RandomVariable, AbsBaseParameter, ParameterSpecError, AbsBaseParameterSet, ParameterSetError
from .SymSet import SymSet, SymSetError

_logger = log.getLogger(__name__)


def _makeKey(name, row, col, dropZeros=False):
    '''
    Convenience function for cases of 2 dimensions, row & col.
    '''
    return makeDistroKey(name, [row, col], dropZeros=dropZeros)


class MatrixRV(RandomVariable):
    '''
    Each instance holds a named random variable and an array of its values,
    generated in advance using LHS, and accounting for correlations. A row or col
    of zero means the entire row or column, respectively, is modified by this RV.
    '''
    def __init__(self, distro, name, row=0, col=0):
        super(MatrixRV, self).__init__(distro, name)
        self.row    = row   # the starting row and columm modified by this RV ...
        self.col    = col   # ... zero means the entire row or column

    def key(self):
        'Produce a string key from the parameter name, row, and column'
        return _makeKey(self.name, self.row, self.col)

class MatrixParameter(AbsBaseParameter):
    '''
    Stores information about a single model parameter. Value is a DataFrame or matrix of floats.
    This should not be instantiated directly; it is used only by the readFile() method.
    Sets is a list of SymSets associated with the axes of a parameter. An axis with
    a symbolic set associated with it can be indexed either by members of the set or
    integers directly.
    '''
    def __init__(self, name, data, dtype, sets):
        super(MatrixParameter, self).__init__(name, data, dtype)

        # Data is assumed to be np.ndarray
        self.rows, self.cols = data.shape

        if not sets:
            try:
                # the elements of metaSet are the names of sets defining dimensions
                metaSet = SymSet.get(name)

            except SymSetError:
                metaSet = None

            if metaSet:
                sets = map(SymSet.get, metaSet.getSymbols())
            else:
                sets = []

        self.sets = sets

    def __str__(self):
        return "<%s %s %sx%s of %s at 0x%x>" % \
            (self.__class__.__name__, self.name, self.rows, self.cols, self.dtype.__name__, id(self))

    # TBD: maybe replace with method in ilucmcs.Package, which uses DataFrames?
    @classmethod
    def readFile(cls, filename):
        '''
        Read all header descriptions and data from the file. "Matrix" data is stored as a
        list of lists. Return a dict of Parameter instances, keyed by name.
        '''
        paramDict = OrderedDict()
        skip_until = 0
        rows = 0
        rowCount = 0
        cols = 0
        name = None
        data = []
        sets = []
        for lineNum, items in enumerate(fileReader(filename, error=ParameterSpecError)):
            if skip_until > lineNum:  # Skipped line
                continue
            if items[0] == 'SKIP':
                try:
                    skipRows = int(items[1])
                except (ValueError, IndexError):
                    raise ParameterSpecError('Malformed skip statement. Must have integer argument for number of rows.')
                skip_until = lineNum + skipRows + 1
                continue

            if not is_number(items[0]):  # indicates the line is a header
                if len(items) != 3:
                    raise ParameterSpecError('Malformed header. Not enough or too many arguments.')
                if rowCount < rows:
                    raise ParameterSpecError('Not enough rows. Found %i, should be %i.' % (rowCount, rows))
                name = items[0]

                try:
                    rows = int(items[1])
                    sets.append(None)
                except ValueError:
                    rowSet = SymSet.get(items[1])
                    rows = len(rowSet.symbols)
                    sets.append(rowSet)

                try:
                    cols = int(items[2])
                    sets.append(None)
                except ValueError:
                    colSet = SymSet.get(items[2])
                    cols = len(colSet.symbols)
                    sets.append(colSet)

            else:  # line must be a data row, because titles can't be numbers
                rowCount += 1  # rowCount is number of rows since last header
                colCount = len(items)
                if colCount != cols:
                    raise ParameterSpecError('Wrong number of columns. Found %i, should be %i.' % (colCount, cols))
                if rowCount > rows:
                    raise ParameterSpecError('Too many rows. Found %i, should be %i' % (rowCount, rows))

                dtype = float   # base parameter class deals only in floats
                data.append(map(float, items))

                if rowCount == rows:
                    dataArray = np.array(data)
                    param = cls(name, dataArray, dtype, sets)
                    rows = 0
                    rowCount = 0
                    data = []
                    sets = []
                    paramDict[name] = param

        if data:
            raise ParameterSpecError('Not enough rows. Found %i, should be %i.' % (rowCount, rows))

        return paramDict

    def _writeData(self, stream, delimiter="\t"):
        fmt = "%g" if self.dtype == float else "%s"
        np.savetxt(stream, self.data, fmt=fmt, delimiter=delimiter)
        stream.write("\n")  # might be unnecessary

    def _writeHeader(self, stream, delimiter="\t"):
        set1, set2 = self.sets
        setName1 = (set1 and set1.name) or str(self.rows)
        setName2 = (set2 and set2.name) or str(self.cols)
        stream.write(delimiter.join((self.name, setName1, setName2)))
        stream.write("\n")

    def write(self, stream):
        '''
        Writes a parameter to a stream, keeping the data and
        setnames/dimension values intact.

        Stream is an object supporting the "write" method.
        '''
        self._writeHeader(stream)
        self._writeData(stream)

#
# Private functions used in genTrial, but made global to avoid
# redefining them on each call to that method.
#
def _updateDirect(data, row, col, value, updateZero, _low, _high):
    # low and high are unused in this case
    if updateZero or data[row, col] != 0:
        data[row, col] = value


def _updateFactor(data, row, col, value, _updateZero, low, high):
    '''
    Update a value by multiplying the random variable to the default,
    respecting any given upper or lower bound.
    '''
    data[row, col] = min(max(value * data[row, col], low), high)


def _updateDelta(data, row, col, value, _updateZero, low, high):
    '''
    Update a value by adding the random variable to the default,
    respecting any given upper or lower bound.
    '''
    data[row, col] = min(max(value + data[row, col], low), high)


class MatrixParameterSet(AbsBaseParameterSet):
    '''
    Routines to read and write text-format parameter files. Parameter and Distro classes
    specify what class to use for reading of parameter/distro files.
    '''
    def __init__(self, program, paramFile, distFile):
        super(MatrixParameterSet, self).__init__(program, paramFile, distFile, self.parameterClass())

    @classmethod
    def parameterClass(cls):
        return MatrixParameter

    def _trialValue(self, trialNum, name, row=0, col=0):
        key = _makeKey(name, row, col)
        rv = self.rvDict[key]
        return rv.trialValue(trialNum)

    def readDistroFile(self, distFile, paramDict, distroClass):
        '''
        Read a distribution file and return two items: (i) a dict keyed by
        short name of the distributions read from the file, and (ii) a list
        of CorrelationDef instances.
        '''
        return distroClass.readFile(distFile, paramDict, allowDups=True)

    def genCorrPairs(self, corrDefs):
        '''
        Replace definitions from distroObj file with specific pairwise
        correlations and return a list of corrPair instances.
        '''
        corrPairs = []

        for corrDef in corrDefs:
            name1 = corrDef.name1
            name2 = corrDef.name2
            coef = corrDef.coef

            param1 = self.paramDict[name1]
            distros1 = self.distroDict[name1]
            distro1 = distros1[0]
            target1 = distro1.target

            # default values (which work unmodified for target 'cells')
            startRow = startCol = 1
            rows = param1.rows
            cols = param1.cols

            if target1 == 'index':
                # dimIndexes is a list of dimension indices; the first is the row, the second is the column
                startRow = distro1.dimIndexes[0]
                startCol = distro1.dimIndexes[1]
                if startCol == 0:
                    cols = 1  # only row indexed
                if startRow == 0:
                    rows = 1  # only col indexed
            elif target1 == 'cols':
                startRow = 0
                rows = 1
            elif target1 == 'rows':
                startCol = 0
                cols = 1
            elif target1 == 'single':
                rows = cols = 1

            # Loops run until these limits
            limitRow = startRow + rows
            limitCol = startCol + cols

            if name2:
                # Verify that the two parameters have the same shape
                param2 = self.paramDict[name2]
                if param1.rows != param2.rows:
                    raise ParameterSetError("Can't correlate %s and %s: rows differ" % (name1, name2))
                if param1.cols != param2.cols:
                    raise ParameterSetError("Can't correlate %s and %s: cols differ" % (name1, name2))

                distros2 = self.distroDict[name2]
                if not (len(distros1) == 1) or not(len(distros2) == 1):
                    raise ParameterSetError("Can't correlate %s and %s: multiple distributions" % (name1, name2))

                # Make sure the distros are defined for the same target
                target2 = distros2[0].target
                if not (target1 == target2):
                    raise ParameterSetError("Can't correlate RVs (%s and %s) with different targets (%s and %s)" \
                                            % (name1, name2, target1, target2))

                for row in xrange(startRow, limitRow):
                    for col in xrange(startCol, limitCol):
                        # Create correlation between corresponding elements of each matrix
                        key1 = _makeKey(name1, row, col)
                        key2 = _makeKey(name2, row, col)
                        corrPair = CorrelationPair(key1, key2, coef)
                        corrPairs.append(corrPair)
            else:
                # Handle intra-matrix correlations: loop over the matrix,
                # generating all possible pair-wise correlations among
                # elements, other than correlating an element with itself.
                for row1 in xrange(startRow, limitRow):
                    for col1 in xrange(startCol, limitCol):
                        key1 = _makeKey(name1, row1, col1)

                        for row2 in xrange(row1, limitRow):
                            for col2 in xrange(col1, limitCol):
                                if row1 == row2 and col1 == col2:
                                    continue

                                key2 = _makeKey(name1, row2, col2)
                                corrPair = CorrelationPair(key1, key2, coef)
                                corrPairs.append(corrPair)
        return corrPairs

    def genRanVars(self):
        '''
        Generates and returns a list of the MatrixRV instances required
        to meet the Parameter definitions as per the parameter and distribution
        files. This list is then traversed to generate values using LHS.
        '''
        rvDict = OrderedDict()

        for name, param in self.paramDict.iteritems():
            distros = self.distroDict[name]

            if not distros:
                continue

            rows = param.rows
            cols = param.cols

            #
            # There can be multiple distributions defined for a single named parameter,
            # applying to specific rows, columns, or cells, overriding previous values.
            #
            for distro in distros:
                target = distro.target

                if target == 'rows':  # generate a different random value per row
                    for row in xrange(1, rows + 1):
                        rv = MatrixRV(distro, name, row=row)
                        rvDict[rv.key()] = rv

                elif target == 'cols':  # generate a different random value per col
                    for col in xrange(1, cols + 1):
                        rv = MatrixRV(distro, name, col=col)
                        rvDict[rv.key()] = rv

                elif target == 'cells':  # generate a different random value for each cell
                    for row in xrange(1, rows + 1):
                        for col in xrange(1, cols + 1):
                            rv = MatrixRV(distro, name, row=row, col=col)
                            rvDict[rv.key()] = rv

                elif target == 'single':  # generate a single random value for all cells
                    rv = MatrixRV(distro, name, row=1, col=1)
                    rvDict[rv.key()] = rv

                elif target == 'index':  # this target is set in Distro.py when indexed params are used
                    row = distro.dimIndexes[0]
                    col = distro.dimIndexes[1] if len(distro.dimIndexes) > 1 else 0
                    rv = MatrixRV(distro, name, row=row, col=col)
                    rvDict[rv.key()] = rv
        return rvDict

    def getTrialDataLoop(self, simId, trialNum, inValueDict, session):
        '''
        This is the inner loop of getTrialData, which can be customized to
        accommodate different parameter forms, e.g., matrices, lists, etc.
        '''

        paramsCopy = []

        for name, param in self.paramDict.iteritems():
            distros = self.distroDict[name]

            # Copy on write: if not changing anything, reference the original object
            if not distros:
                paramsCopy.append(param)
                # print "getTrialData: no distribution defined for %r" % sname
                continue

            #log.debug("getTrialDataLoop: generating data for %s" % name)

            # Otherwise, deepcopy the parameter and alter it rather than the original
            param = copy.deepcopy(param)
            paramsCopy.append(param)

            rows, cols, data = param.rows, param.cols, param.data

            paramList = session.query(Input).filter_by(programId=self.programId, paramName=name).all()
            if paramList:
                assert len(paramList) == 1, "Input table has multiple entries with same {programId,name} combination"
                inputId = paramList[0].inputId
            else:
                newParam = Input(programId=self.programId, paramName=name)
                session.add(newParam)
                session.commit()
                inputId = newParam.inputId

            def _makeInValue(row, col, value):
                '''
                Store input value records under a hashkey of inputId-row-col-simId-trialNum, so that
                subsequent definitions overriding prior ones, as desired. At the end, we walk
                the dictionary and insert all records in one transaction.
                '''
                newInValue = InValue(inputId=inputId, simId=simId, trialNum=trialNum,
                                     row=row, col=col, value=value)
                key = '%d-%d-%d-%d-%d' % (inputId, row, col, simId, trialNum)
                inValueDict[key] = newInValue

            #
            # We pre-generate all parameter values for all trials, observing the correlations
            # within matrix parameters. Then we call extract the value from each for the given
            # trialNum, e.g.,
            #     value = distDef.getValue(trialNum, row, col)
            # If Target is 'Single', ignore row and col (or treat these as 1)
            # If Target is 'Row', ignore col, and vice-versa
            # If Target is 'Cells', use both row and col.
            #
            # There can be multiple distributions defined for a single named parameter, which
            # are applied to specific rows, columns, or cells, overriding previous values.
            #
            for distDef in distros:
                target = distDef.target
                distType = distDef.distType  # these are lower-cased in the distDef
                modDict  = distDef.modDict
                updateZero = bool(modDict['_updatezero'])

                # We don't change zero values except for binary variables, unless specified
                if distType == 'binary':
                    updateZero = True

                # Determine which update function to use, avoiding tests within the loop
                updateFn = _updateFactor if distDef.isFactor else (_updateDelta if distDef.isDelta else _updateDirect)

                # For factor and delta: if modif gives a low and/or high bound, ensure that
                # results of applying the RV respects the bound.
                low  = float('-inf')
                high = float('inf')

                if distDef.isFactor or distDef.isDelta:     # TBD: test this
                    if LOWBOUND_MODIFIER in modDict:  # check presence to avoid misinterpreting a zero
                        low = float(modDict[LOWBOUND_MODIFIER])

                    if HIGHBOUND_MODIFIER in modDict:
                        high = float(modDict[HIGHBOUND_MODIFIER])

                    if low > high:
                        raise ValueError('Lower bound is greater than upper bound.')

                if target == 'rows':  # generate a different random value per row
                    for row in xrange(rows):
                        value = self._trialValue(trialNum, name, row=row + 1, col=0)
                        _makeInValue(row=row + 1, col=0, value=value)
                        for col in xrange(cols):
                            updateFn(data, row, col, value, updateZero, low, high)

                elif target == 'cols':  # generate a different random value per col
                    for col in xrange(cols):
                        value = self._trialValue(trialNum, name, row=0, col=col + 1)
                        _makeInValue(row=0, col=col + 1, value=value)
                        for row in xrange(rows):
                            updateFn(data, row, col, value, updateZero, low, high)

                elif target == 'cells':  # generate a different random value for each cell
                    for row in xrange(rows):
                        for col in xrange(cols):
                            value = self._trialValue(trialNum, name, row=row + 1, col=col + 1)
                            _makeInValue(row=row + 1, col=col + 1, value=value)
                            updateFn(data, row, col, value, updateZero, low, high)

                elif target == 'single':  # generate a single random value for all cells
                    value = self._trialValue(trialNum, name, row=1, col=1)
                    _makeInValue(row=0, col=0, value=value)
                    for row in xrange(rows):
                        for col in xrange(cols):
                            updateFn(data, row, col, value, updateZero, low, high)

                elif target == 'index':  # this target is set in Distro.py for indexed params
                    row = distDef.dimIndexes[0]  # NOTE: these values are 1-relative as in GTAP
                    col = distDef.dimIndexes[1] if len(distDef.dimIndexes) > 1 else 0
                    value = self._trialValue(trialNum, name, row=row, col=col)
                    _makeInValue(row=row, col=col, value=value)
                    # If both a row and column are specified (these are 1-relative)
                    # Convert them to zero-relative for use with updateFn()
                    row -= 1
                    col -= 1
                    if row >= 0 and col >= 0:
                        updateFn(data, row, col, value, updateZero, low, high)

                    elif row >= 0:  # only row is specified, update entire column
                        for col in xrange(cols):
                            updateFn(data, row, col, value, updateZero, low, high)

                    else:  # only col is specified, update entire row
                        for row in xrange(rows):
                            updateFn(data, row, col, value, updateZero, low, high)
        return paramsCopy
