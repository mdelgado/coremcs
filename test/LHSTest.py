'''
Created on Nov 23, 2012

@author: Sam Fendell
@author: Richard Plevin

This is incomplete -- there is a confusion here between the Distro class, which stores
information about a distribution, and the Parameter class, which holds data and metadata
about a Parameter. A ParameterSet combines distribution and Parameter information.
'''
import unittest
from random import randrange, random
import numpy as np
from coremcs.LHS import lhs, rankCorrCoef
from coremcs.MatrixParameter import MatrixParameter
from coremcs.Distro import Distro, genDistros
from coremcs.Configuration import getConfigManager, CoreConfiguration

def printMat(m):
    for row in m:
        for value in row:
            print "%6.3f " % value,
        print ""
    print ""


class TestParam(MatrixParameter):
    def __init__(self, name, data):
        sets = None
        super(TestParam, self).__init__(name, data, sets)


class TestDistro(Distro):
    '''
    Convenience class to compute some of the parameters to Distro(), just for testing.
    It also stands in for a subclass of MatrixParameter by virtue of providing the ppf function.
    '''
    def __init__(self, distName, argDict):
        name   = '%s%s' % (distName, argDict)
        data   = np.random.rand(6,6)
        param  = TestParam(name, data)
        target = 'Single'
        distType  = distName
        dim_names = None
        modDict   = {}
        super(TestDistro, self).__init__(param, target, distType, dim_names, argDict, modDict)

    # Unclear why this is in the distro class
    def ppf(self, *args):
        "Pass-thru to underlying RV's ppf"
        return self.rv.ppf(*args)


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @classmethod
    def setUpClass(cls):
        getConfigManager(CoreConfiguration)
        genDistros()

    def testConstructedMatrices(self):

        plist = [TestDistro('Normal',    {'mean': 20, 'std':3}),
                 TestDistro('Normal',    {'mean': 20, 'std':3}),
                 TestDistro('Lognormal', {'mean': 50, 'std':10}),
                 TestDistro('Lognormal', {'mean': 50, 'std':10}),
                 TestDistro('Triangle',  {'min':  10, 'mode':50, 'max':100}),
                 TestDistro('Uniform',   {'min':  100,'max': 131})
                 ]
        m = np.array([[1.00,  0.33,  0.00,  0.00,  0.50,  0.10],
                      [0.33,  1.00,  0.00,  0.00,  0.50,  0.20],
                      [0.00,  0.00,  1.00,  0.90,  0.50,  0.30],
                      [0.00,  0.00,  0.90,  1.00,  0.50,  0.40],
                      [0.50,  0.50,  0.50,  0.50,  1.00,  0.50],
                      [0.10,  0.20,  0.30,  0.40,  0.50,  1.00]])

        for _ in range(10):
            self._testLHS(plist, m)


    def skip_testRandomMatrices(self):
        goodRes = 0
        tests   = 100
        numVars = 10

        for _ in range(tests):
            plist = [TestDistro('uniform',   {'min'   : randrange(-100,0),       'max'    : randrange(0,100)}),
                     TestDistro('normal',    {'mean'  : randrange(-50,50),       'std'    : randrange(-10,10)}),
                     TestDistro('lognormal', {'mean'  : randrange(2,10)+random(),'std'    : random()}),
                     TestDistro('lognormal', {'low95' : randrange(1,10),         'high95' : randrange(90,100)}),
                     TestDistro('triangle',  {'min'   : randrange(-150,-49),     'mode'   : randrange(-50,50),    'max':randrange(51,150)})
                     ]

            # Choose numVars (10) random correlations. If we set too many,
            # it's more likeley that some cases will be internally inconsistent.
            choices = [np.random.choice(numVars, size=2, replace=False) for _ in range(numVars)]
            corr = np.zeros((numVars,numVars))

            for i, j in choices:
                randomSign = 1 if np.random.choice(2) else -1
                value = np.random.random(1) * randomSign
                corr[i][j] = value
                corr[j][i] = value


            #corr = np.random.random_sample(size=(numVars,numVars))
            #corr = (corr + corr.T)/2
            corr = corr*corr.T
            #corr = np.corrcoef(corr)

            printMat(corr)

#             try:
#                 np.linalg.cholesky(corr)
#             except:
#                 print "linalg.cholesky(corr) failed"
#                 continue
#             goodRes+=1

            # Choose 10 times from the list to create a list of "variables"
            newPlist = [np.random.choice(plist) for _ in range(numVars)]

            self._testLHS(newPlist, corr, printMats=True)
#            print goodRes, "  were good"

        self.assertEqual(goodRes, tests)


    def _testLHS(self, plist, corrMat, printMats=False):
        '''
        Ask for LHS to produce data with a given rank correlation, then check to
        see if the values are within epsilon, since this is an approximate process.
        '''
        epsilon = 0.10

        # The parameter names are not needed here, and the paramList should include the
        # frozen scipy distribution objects that can return inverse lookups via ppf.
        c = lhs(plist, trials=1000, corrMat=corrMat)

        corrs = rankCorrCoef(c)
        if printMats:
            print 'Requested correlations:'
            printMat(corrMat)
            print 'Actual correlations:\n'
            printMat(corrs)

        for testRow,resRow in zip(corrMat, corrs):
            for testVal,resVal in zip(testRow,resRow):
                self.assertTrue(abs(testVal - resVal) < epsilon,
                                "Difference > epsilon: testVal=%3.3f, resVal=%3.3f, epsilon=%2.2f" % (testVal, resVal, epsilon))
