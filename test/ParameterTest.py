'''
Created on Jan 7, 2013

@author: SAM FENDELL
'''
import unittest
import os
from coremcs.MatrixParameter import MatrixParameter, ParameterSpecError
from coremcs.SymSet import SymSet


class Test(unittest.TestCase):

    def setUp(self):
        SymSet.readFile('./ParameterTestData/paramSets.set')
        filename = './ParameterTestData/testGoodParams.prm'
        self.paramDict = MatrixParameter.readFile(filename)

    def tearDown(self):
        SymSet.clearSets()

    def testGoodParams(self):
        params = self.paramDict

        assert len(params) == 6

        paramList = params.values()
        param = paramList[0]
        assert param.name == 'test1'
        assert param.rows == 3
        assert param.cols == 1
        assert param.data == [[6], [7], [-1]]
        assert param == params[param.name]

        param = paramList[1]
        assert param.name == 'test2'
        assert param.rows == 1
        assert param.cols == 1
        assert param.data == [[9]]
        assert param == params[param.name]

        param = paramList[2]
        assert param.name == 'test3'
        assert param.rows == 3
        assert param.cols == 4
        assert param.data == [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
        assert param == params[param.name]

        param = paramList[3]
        assert param.name == 'test4'
        assert param.rows == 1
        assert param.cols == 3
        assert param.data == [[-1, 2, 3]]
        assert param == params[param.name]

        param = paramList[4]
        assert param.name == 'test5'
        assert param.rows == 2
        assert param.cols == 2
        assert param.data == [[2, 2], [2, 2]]
        assert param == params[param.name]

        param = paramList[5]
        assert param.name == 'test6'
        assert param.rows == 3
        assert param.cols == 1
        assert param.data == [[1], [2], [3]]
        assert param == params[param.name]

    def testParamsWithSymbolicRowAndCol(self):

        SymSet.readFile('./ParameterTestData/paramSets.set')
        pdict = MatrixParameter.readFile('./ParameterTestData/testSetParams.prm')
        plist = pdict.values()
        param = plist[0]
        self.assertEquals(2, param.rows)
        self.assertEquals(3, param.cols)

    def testParamsWithSymbolicRow(self):

        SymSet.readFile('./ParameterTestData/paramSets.set')
        pdict = MatrixParameter.readFile('./ParameterTestData/testSetParams.prm')
        plist = pdict.values()
        param = plist[1]
        self.assertEquals(3, param.rows)
        self.assertEquals(4, param.cols)

    def testParamsWithSymbolicCol(self):

        SymSet.readFile('./ParameterTestData/paramSets.set')
        pdict = MatrixParameter.readFile('./ParameterTestData/testSetParams.prm')
        plist = pdict.values()
        param = plist[2]
        self.assertEquals(2, param.rows)
        self.assertEquals(2, param.cols)

    def testBadDistros(self):
        dirr = './ParameterTestData/badParameters'
        for f in os.listdir(dirr):
            if not f.startswith('.'):
                filename = os.path.join(dirr, f)
                self.assertRaises(ParameterSpecError, MatrixParameter.readFile, filename)

    def testWriteFile(self):
        MatrixParameter.writeFile('ParameterTestData/testWriteFile.prm', self.paramDict.values())
        self.setUp()
        self.testGoodParams()
