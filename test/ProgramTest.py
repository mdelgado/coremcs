#!/usr/bin/env python

from time import sleep
import logging

_logger = logging.getLogger(__name__)

def runner(_args):
    '''
    This function will be called directly by Runner.py. We ignore args for now.
    '''
    _logger.info("Running %s..." % __file__)
    sleep(3)
    _logger.info("Exiting %s" % __file__)
    return 0
