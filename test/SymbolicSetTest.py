'''
Created on Dec 16, 2012

@author: Sam Fendell
'''
import unittest
from coremcs.SymSet import SymSet, SymSetError


class Test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        SymSet.readFile('./SymbolicSetTestData/TestSets.set')
        SymSet.readFile('./SymbolicSetTestData/IrregularWhitespace.set')


    def testStartOption(self):
        regSymSet = SymSet.get('test1')
        self.assertEquals(regSymSet.range(), (3, 6))
        self.assertEquals(regSymSet.count(), 3)
        self.assertEquals((regSymSet['symbol1'], regSymSet['symbol2'], regSymSet['symbol3']), (3, 4, 5))

    def testNoOptions(self):
        cropSymSet = SymSet.get('test2')
        self.assertEquals(cropSymSet.range(), (1, 7))
        self.assertEquals(cropSymSet.count(), 6)
        for x in range(1, 6):
            self.assertEquals(cropSymSet[str(x)], x)

    def testOptionalIndices(self):
        testSet = SymSet.get('test3')
        self.assertEquals(testSet.range(), (10, 13))
        self.assertEquals(testSet.count(), 3)

    def testExceptions(self):
        self.assertRaises(SymSetError, SymSet.get, "doesn't exist")
        self.assertRaises(SymSetError, SymSet.symbolValue, "REG", " not a real symbol")

    def testIrregularWhitespace(self):
        set1 = SymSet.get('test4')
        self.assertEquals(set1.range(), (1, 6))
        self.assertEquals(set1.count(), 5)
        self.assertEquals((set1['symbol1'], set1['symbol2']), (1, 2))
        self.assertEquals((set1['symbol3'], set1['symbol4'], set1['symbol5']), (3, 4, 5))

        set2 = SymSet.get('test5')
        self.assertEquals(set2.range(), (5, 7))
        self.assertEquals((set2['symbol5'], set2['symbol6']), (5, 6))
        self.assertEquals((set2['extraSymbol'], set2['extraSymbol2']), (6, 7))
