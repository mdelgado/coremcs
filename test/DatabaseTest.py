'''
Created on Jan 4, 2013

@author: Sam Fendell
@author: Richard Plevin
'''
import unittest, time, thread, threading
from coremcs.Configuration import getConfigManager, CoreConfiguration, DEFAULT_SECTION, APP_NAME_OPTION
from coremcs.Database import getSession, getDatabase, Code, Run, Sim, Output, OutValue, RunStatus, \
    DFLT_PROGRAM, RUN_QUEUED, RUN_RUNNING, RUN_SUCCEEDED

APP_NAME = 'mcstest'
EXP_NAME = 'test-exp'

# TODO: this needs to be brought into SQLAlchemy
def sleepDB(sem):
    db = getDatabase()
    print 'got to sleepDb'
    db.execute('begin transaction;')
    db.executeWithRetry('UPDATE test SET testcol=1 WHERE testcol=2;')
    sem.release()
    time.sleep(10)
    print 'gonna end transaction'
    db.execute('end transaction;')


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        'Push the AppName "mcstest" into the config parser since this is also the database name.'
        cfg = getConfigManager(CoreConfiguration)
        cfg.parser.set(DEFAULT_SECTION, APP_NAME_OPTION, APP_NAME)

    #
    # The test routines are named with letters since order matters and they're run alphabetically
    #

    def test_A_createDb(self):
        self.db = getDatabase()
        self.db.dropAll()
        self.db.initDb()

    def test_B_query(self):
        db = getDatabase()
        session = getSession()

        statusCodes = {}
        for name, value in session.query(Code.codeName, Code.codeId):
            statusCodes[name] = value

        self.assertEqual(len(statusCodes), 5, 'Expected 5 rows returned from query; got %d' % len(statusCodes))
        self.assertTrue(statusCodes['queued'] == 0 and statusCodes['killed'] == -2), 'Status code values are incorrect'

        #df = db.queryToDataFrame('select * from code order by codeId')
        df = db.queryToDataFrame('SELECT * FROM code')
        self.assertEquals(df[df.codeName == RUN_RUNNING].codeId.values, RunStatus[RUN_RUNNING])


    def test_C_createSim(self):
        db = getDatabase()
        session = getSession()

        trials = 5
        description = 'Test sim'
        simId = db.createSim(trials, description, simId=None)
        self.assertEquals(simId, 1)
        rslt  = session.query(Sim).one()
        self.assertEqual(rslt.trials, trials)
        self.assertEqual(rslt.description, description)


    def test_D_experiment(self):
        expName = EXP_NAME
        description = 'An experiment for unit testing'
        db = getDatabase()
        expId = db.createExp(expName, description)
        self.assertEqual(expId, 1)
        self.assertEqual(db.getExpId(expName), expId)


    def test_E_startRun(self):
        simId    = 1
        trialNum = 30
        expName  = EXP_NAME
        db       = getDatabase()
        runId    = db.createRun(simId, trialNum, expName=expName)
        #print "Created run with runId %d" % runId
        session  = getSession()

        rslt = session.query(Run).get(runId)
        self.assertTrue(rslt.queueTime is not None)
        self.assertEqual(rslt.runStatus, db.getRunStatusCode(RUN_QUEUED))
        session.commit() # subsequent query sees updated state

        db.setRunStatus(runId, RUN_RUNNING)
        rslt = session.query(Run).one()

        self.assertEqual(rslt.runId, runId)
        self.assertEqual(rslt.simId, simId)
        self.assertEqual(rslt.trialNum, trialNum)
        #print 'code for status %s is %d' % (RUN_RUNNING, db.getRunStatusCode(RUN_RUNNING))
        #print 'result status is %d' % rslt.runStatus
        self.assertEqual(rslt.runStatus, db.getRunStatusCode(RUN_RUNNING))
        self.assertTrue(rslt.startTime is not None)


    def test_F_createAttr(self):
        attrName = 'appStatus'
        attrType = 'number'
        description = 'Application-specific status code'

        db = getDatabase()
        db.createAttribute(attrName, attrType, description)

        # Set the appStatus for the one run we've created
        numValue = -10
        runId    = 1
        db.setAttrVal(runId, attrName, numValue=numValue, strValue=None, session=None)


    def test_G_saveResult(self):
        db = getDatabase()

        db.createOutput('p1', description='Test parameter 1', program=DFLT_PROGRAM)
        db.createOutput('p2', description='Test parameter 2', program=DFLT_PROGRAM)
        db.createOutput('p3')   # test defaults

        session = getSession()
        count = session.query(Output).count()
        self.assertEqual(count, 3)


    def test_H_saveResult(self):
        simId    = 1
        trialNum = 44
        expName  = EXP_NAME
        db       = getDatabase()
        runId    = db.createRun(simId, trialNum, expName=expName)
        session  = getSession()
        value    = 10
        db.setOutValue(runId, 'p1', value)

        run    = db.getRunObj(runId, session)
        tuples = db.getOutValues(run.simId, EXP_NAME, 'p1')
        self.assertEqual(len(tuples), 1)

        saved = tuples[0].value
        self.assertEqual(value, saved)

        # Test saving multiple rows in one transaction
        value = 20
        db.setOutValue(runId, 'p1', value,   session=session)    # overwrites old value
        db.setOutValue(runId, 'p2', value+1, session=session)
        db.setOutValue(runId, 'p3', value+2, session=session)
        session.commit()

        saved = session.query(OutValue.value).filter_by(runId=runId).join(Output).filter_by(name='p1').scalar()
        self.assertEqual(saved, value)


    def test_I_getOutvalues(self):
        db = getDatabase()

        # Create another run and outvalue
        simId    = 1
        trialNum = 999
        expName  = EXP_NAME
        value    = 100
        myRunId  = db.createRun(simId, trialNum, expName)
        db.setOutValue(myRunId, 'p1', value)    # overwrites old value

        _rslt = db.getOutValues(simId, EXP_NAME, 'p1')
        #for trialNum, value in rslt:
        #    print "TrialNum: %d, value: %f" % (trialNum, value)

    def test_J_getRunsWithStatus(self):
        simId    = 1
        trialNum = 999
        expName  = EXP_NAME

        db = getDatabase()
        runId = db.createRun(simId, trialNum, expName)
        db.setRunStatus(runId, RUN_SUCCEEDED)

        _rslt = db.getRunsWithStatus(simId, 'test-exp', RUN_QUEUED)
        _rslt = db.getRunsWithStatus(simId, 'test-exp', RUN_SUCCEEDED)


    def test_Z_finishRun(self):
        sleep  = 0
        if sleep:
            print "Delaying before finishing run"
            time.sleep(5)

        db = getDatabase()
        session = getSession()

        simId    = 1
        trialNum = 999
        expName  = EXP_NAME

        runId = db.createRun(simId, trialNum, expName)

        db.setRunStatus(runId, RUN_SUCCEEDED)
        run   = db.getRunObj(runId, session)
        self.assertEqual(run.runStatus, db.getRunStatusCode(RUN_SUCCEEDED), "Run status %d is not RUN_SUCCEEDED (%s)" % (run.runStatus, RUN_SUCCEEDED))
        self.assertTrue(run.endTime is not None)

    # TODO: get this working
    def XXX_testExecuteWithRetry(self):
        self.db.executeWithRetry('CREATE TABLE test (testcol int);')
        print 'table created'
        self.db.execute('INSERT INTO test VALUES(2);')
        print 'test entries added'
        sem = threading.Semaphore(0)
        thread.start_new_thread(sleepDB, (self.dbFile, sem))
        print 'about to acquire semaphore in main thread...'
        sem.acquire()
        print 'sem acquired'
        self.db.executeWithRetry('UPDATE test SET testcol=0 WHERE testcol=2;')
        data = self.db.fetch('SELECT * from test;')
        print data
        print 'got here too'
        self.assertEqual(data, [(17,)])
