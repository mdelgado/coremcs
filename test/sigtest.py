from signal import signal, alarm, SIGTERM, SIGQUIT, SIGALRM
from time import sleep

class TimeoutError(Exception):
    pass

class AlarmError(Exception):
    pass

# We catch only these 3 signals. Can extend this if needed.
def signame(signum):
    if signum == SIGTERM:
        return 'SIGTERM'

    if signum == SIGQUIT:
        return 'SIGQUIT'

    if signum == SIGALRM:
        return 'SIGALRM'

    return 'signal %d' % signum


def sighandler(signum, _frame):
    if signum == SIGALRM:
        raise AlarmError('Alarm clock terminated trial')
    else:
        raise TimeoutError('Process received %s' % signame(signum))

signal(SIGTERM, sighandler)
signal(SIGQUIT, sighandler)
signal(SIGALRM, sighandler)

try:
    alarm(10)
    sleep(15)
except Exception, e:
    print e

print "Exiting"
