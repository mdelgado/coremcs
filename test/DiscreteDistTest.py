'''
Created on Dec 20, 2012

@author: SAM FENDELL
'''
import unittest
from coremcs.DiscreteDist import DiscreteDist, DiscreteDistError


class Test(unittest.TestCase):

    def testSmallDistro(self):
        smallProbs = [(50, .3), (100, .3), (120, .4)]
        smallDistro = DiscreteDist(smallProbs)
        values = smallDistro.rvs(1000)
        count50 = values.count(50)
        count100 = values.count(100)
        count120 = values.count(120)
        self.assertAlmostEqual(300, count50, delta=100)
        self.assertAlmostEqual(300, count100, delta=100)
        self.assertAlmostEqual(400, count120, delta=100)
        self.assertEquals(1000, count50 + count100 + count120)
        self.assertEquals(1000, len(values))

    def testExceptions(self):
        probs = [(1, 0.5), (2, 0.6)]
        try:
            DiscreteDist(probs)
            self.fail()
        except Exception as e:
            self.assertEquals('Sum of probabilities != 1 (sum=%f). Try setting the tolerance higher.' % 1.1, str(e))
        probs = [(1, 0.2)]
        try:
            DiscreteDist(probs)
            self.fail()
        except DiscreteDistError as e:
            self.assertEquals('Sum of probabilities != 1 (sum=%f). Try setting the tolerance higher.' % 0.2,
                               str(e))

    def testTolerance(self):
        probs = [(1, 0.6), (2, 0.6)]
        distro = DiscreteDist(probs, tolerance=0.2)
        try:
            DiscreteDist(probs, tolerance=0.19)
            self.fail()
        except DiscreteDistError as e:
            self.assertEquals('Sum of probabilities != 1 (sum=%f). Try setting the tolerance higher.' % 1.2,
                              str(e))
        values = distro.rvs(1000)
        count1 = values.count(1)
        count2 = values.count(2)
        assert 400 <= count1 <= 600
        assert 400 <= count2 <= 600
        assert count1 + count2 == 1000

    def testPrecision(self):
        probs = [(1, 0.999), (2, 0.001)]
        distro = DiscreteDist(probs, precision=100)
        values = distro.rvs(10000)
        count2 = values.count(2)
        count1 = values.count(1)
        assert 0 <= count2 <= 100
        assert 9900 <= count1 <= 10000

    def testPrecisionAndTolerance(self):
        probs = [(1, 0.509), (2, 0.499)]  # .505, .495 after rescaling
        distro = DiscreteDist(probs, precision=100, tolerance=0.0181)
        values = distro.rvs(100000)
        count1 = values.count(1)
        count2 = values.count(2)
        assert 49500 <= count1 <= 51500
        assert 48500 <= count2 <= 50500
