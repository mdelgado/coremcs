'''
Created on Sep 27, 2013

@author: Richard Plevin
'''
import unittest
from coremcs.Configuration import getConfigManager, CoreConfiguration

class Test(unittest.TestCase):
    def setUp(self):
        self.config = getConfigManager(CoreConfiguration)
        
    def testGetString(self):
        sec = 'DEFAULT'
        val = self.config.getParam('AppRoot', section=sec )
        self.assertTrue(val, 'Config parameter AppRoot was not defined')

    def testGetBoolean(self):
        sec = 'DEFAULT'
        val = self.config.getParamAsBoolean('LogConsole', section=sec)
        self.assertTrue(val is not None, 'Boolean config parameter LogConsole was not defined')

    def testGetInt(self):
        sec = 'DEFAULT'
        val = self.config.getParamAsInt('QueueLimit', section=sec)
        self.assertTrue(val is not None, 'Integer config parameter QueueLimit was not defined')
    
    def testSetAndGet(self):
        var = 'testdefault'
        val = 'modified'
        sec = 'DEFAULT'
        self.config.setParam(var, val, section=sec)     # causes .mcs/mcs.cfg to be saved
        test = self.config.getParam(var, section=sec)
        self.assertEqual(test, val, '%s is %s; expected %s' % (var, test, val))
