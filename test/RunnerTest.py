#!/usr/bin/env python

import os
from coremcs import Runner
import coremcs.util as U
from coremcs.Database import getDatabase, CoreDatabase, RUN_QUEUED
from coremcs.Configuration import getConfigManager, CoreConfiguration


def setupEnviron():
    '''
    setup environment variables
    '''
    # MCS_CFG_CLASS must be in the format, e.g., 'PackageName.ClassName',
    # where the class 'ClassName' is found in package 'PackageName', in
    # the file 'ClassName.py'.

    os.environ['MCS_CFG'] = "coremcs.Configuration.CoreConfiguration"

    # MCS_DB must be in the format described above
    os.environ['MCS_DB'] = "coremcs.Database.CoreDatabase"

    # Comma-delited numbers, or hypen-delimited ranges,as below, so
    # '1,3-5,7-10' is equivalent to '1,3,4,5,7,8,9,10'
    os.environ['MCS_TRIALS'] = "1,3-5"

    # Must contain App name, simId, expName, and what else?
    os.environ['MCS_ARGS'] = "5"

    # The App name, which must be a section in the config file. This is the executable file to run.
    os.environ['MCS_APP'] = "mcstest"

    # Our simulation id and experiment name
    os.environ['MCS_SIMID']   = "1"
    os.environ['MCS_EXPNAME'] = "testExp"

    os.environ['MCS_TIMEOUT_SECONDS'] = "600"


def queueJobs():
    '''
    Simulate queueing these jobs by setting run status to 'queued'
    '''
    context = U.getContext()
    trials  = U.parseTrialString(os.environ.get('MCS_TRIALS'))
    db      = getDatabase(CoreDatabase)

    for trialNum in trials:
        U.setContext(context, trialNum=trialNum)
        run = db.getRunFromContext(context) or db.createRunFromContext(context)
        db.setRunStatus(run.runId, RUN_QUEUED)


getConfigManager(CoreConfiguration)
setupEnviron()
queueJobs()
Runner.main()
