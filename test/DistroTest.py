'''
Created on Jan 1, 2013

@author: Sam Fendell
@author: Richard Plevin
'''
import unittest
import os
from coremcs.Package import CorePackage
from coremcs.Distro import Distro, DistributionSpecError, DistroGen
from coremcs.Configuration import getConfigManager, CoreConfiguration, DEFAULT_SECTION, APP_NAME_OPTION

APP_NAME = 'mcstest'


class MockParam(object):
    def __init__(self, name):
        self.name = name
        self.sets = [None, None]


class MockParamDict(object):
    def __init__(self):
        self.d = {}

    def __contains__(self, item):
        d = self.d
        if item not in d:
            d[item] = MockParam(item)
        return True

    def __getitem__(self, index, **args):
        self.__contains__(index)  # forces it into dict
        return self.d[index]

MockDict = MockParamDict()


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cfg = getConfigManager(CoreConfiguration)
        cfg.parser.set(DEFAULT_SECTION, APP_NAME_OPTION, APP_NAME)
        print "Log level is '%s'" % cfg.getParam('LogLevel')
        CorePackage()

    def testBadDistros(self):
        dirr = './DistroTestData/badDistros'
        filenames = [os.path.join(dirr, f) for f in os.listdir(dirr)]
        for filename in filenames:
            #if filename[0] != '.':
            self.assertRaises(DistributionSpecError, Distro.readFile, filename, MockDict)

    def testCorrelations(self):
        distros, correlations = Distro.readFile('./DistroTestData/testCorrelations.dst', {})
        self.assertTrue(not distros)
        self.assertEqual(3, len(correlations))

    def testDistroGen(self):
        DistroGen('sampleDistroGen', lambda foo, bar: foo + bar)
        distros, _ = Distro.readFile('./DistroTestData/testDistroGen.dst', MockDict)
        self.assertEqual(['SampleParam'], distros.keys())

    def testGoodDiscreteFiles(self):
        distros, _ = Distro.readFile('./DistroTestData/testGoodDistros.dst', MockDict)
        discreteFile = distros['DiscreteFilesTest'][0]
        self.assertEqual({6.5, 7.5}, set(discreteFile.rv.rvs(1000)))

    def testGoodNullDistro(self):
        distros, _ = Distro.readFile('./DistroTestData/testGoodDistros.dst', MockDict)
        self.assertTrue(distros)
        self.assertTrue(not distros['NullTest'])

    def testGoodNoCorrelations(self):
        _, correlations = Distro.readFile('./DistroTestData/testGoodDistros.dst', MockDict)
        self.assertTrue(not correlations)

    def testGoodMultiple(self):
        distros, _ = Distro.readFile('./DistroTestData/testGoodDistros.dst', MockDict)

        modifierTest1 = distros['ModifierTest1'][0]
        modifierTest2 = distros['ModifierTest1'][0]
        modifierTest3 = distros['ModifierTest1'][0]
        self.assertEqual('discrete', modifierTest1.distType)
        self.assertEqual('discrete', modifierTest2.distType)
        self.assertEqual('discrete', modifierTest3.distType)

        self.assertEqual('cells', modifierTest1.target)
        self.assertEqual('cells', modifierTest1.target)
        self.assertEqual('cells', modifierTest1.target)

        self.assertAlmostEqual(500, modifierTest1.rv.rvs(1000).count(0), delta=100)
        self.assertAlmostEqual(500, modifierTest1.rv.rvs(1000).count(1), delta=100)
        self.assertAlmostEqual(500, modifierTest2.rv.rvs(1000).count(0), delta=100)
        self.assertAlmostEqual(500, modifierTest2.rv.rvs(1000).count(1), delta=100)
        self.assertAlmostEqual(500, modifierTest3.rv.rvs(1000).count(0), delta=100)
        self.assertAlmostEqual(500, modifierTest3.rv.rvs(1000).count(1), delta=100)

        discreteInline = distros['DiscreteInlineTest'][0]
        discreteRvs = discreteInline.rv.rvs(1000)
        assert 600 > discreteRvs.count(0) > 400
        assert 600 > discreteRvs.count(5) > 400
        self.assertEqual(set(discreteRvs), {0, 5})

    def testLognormal(self):
        distros, _ = Distro.readFile('./DistroTestData/testLognormalDistro.dst', MockDict)
        distro = distros['LognormalTest'][0]
        self.assertEqual('LognormalTest', distro.name)
        self.assertEqual('single', distro.target)
        self.assertEqual('lognormal', distro.distType)
        self.assertEqual({'mean': 1, 'std': 1}, distro.argDict)

    def testMultipleDistrosSameName(self):
        distros, correlations = Distro.readFile('./DistroTestData/testSameName.dst', MockDict)
        self.assertTrue(not correlations)
        self.assertEqual(['SameName'], distros.keys())
        self.assertEqual(2, len(distros['SameName']))

    def testNormal(self):
        distros, _ = Distro.readFile('./DistroTestData/testNormalDistro.dst', MockDict)
        distro = distros['NormalTest'][0]
        self.assertEqual('NormalTest', distro.name)
        self.assertEqual('cols', distro.target)
        self.assertEqual('normal', distro.distType)
        self.assertEqual({'mean': 7, 'std': 2}, distro.argDict)

    def testNumericIndices(self):
        distros, _ = Distro.readFile('./DistroTestData/testNumericIndices.dst', MockDict)
        dist = distros['NumericIndexTest'][0]
        self.assertEqual(dist.target, 'index')
        self.assertEqual(dist.dim_indxs, [1, 8])

        dist = distros['NumericIndexTest2'][0]
        self.assertEqual(dist.target, 'index')
        self.assertEqual(dist.dim_indxs, [9, 0])

        dist = distros['NumericIndexTest3'][0]
        self.assertEqual(dist.target, 'index')
        self.assertEqual(dist.dim_indxs, [0, 4])

    def testTriangle(self):
        distros, _ = Distro.readFile('./DistroTestData/testTriangleDistro.dst', MockDict)
        distro = distros['TriangleTest'][0]
        self.assertEqual('TriangleTest', distro.name)
        self.assertEqual('cells', distro.target)
        self.assertEqual('triangle', distro.distType)
        self.assertEqual({'min': 1, 'mode': 2, 'max': 3}, distro.argDict)

    def testUniform(self):
        distros, _ = Distro.readFile('./DistroTestData/testUniformDistro.dst', MockDict)
        distro = distros['UniformTest'][0]
        self.assertEqual('UniformTest', distro.name)
        self.assertEqual('rows', distro.target)
        self.assertEqual('uniform', distro.distType)
        self.assertEqual({'min': 6, 'max': 9}, distro.argDict)
