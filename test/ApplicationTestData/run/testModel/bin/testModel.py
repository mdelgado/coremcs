#!/usr/bin/env python
#
# Simplest model to test whether coremcs system is setup correctly.
#
import sys, os, argparse, time
# from coremcs.util import addConsoleHandler

import logging

logfile = 'testModel.log'
logging.basicConfig(filename=logfile, level=logging.DEBUG)
_logger = logging.getLogger(__name__)
# addConsoleHandler(_logger)

def parseArgs():
    parser = argparse.ArgumentParser(description='''Testmodel sleeps the number of seconds
    indicated by the -s argument and then exits with the exit code given by the -e flag.''')

    parser.add_argument('-s', '--seconds', dest='seconds', type=int, required=True,
                        help='The number of seconds to sleep')

    parser.add_argument('-e', '--exitCode', dest='exitCode', type=int, default=0,
                        help='The status code to exit with')

    args = parser.parse_args()
    return args

#
# Main program
#
progName = os.path.basename(sys.argv[0])
args = parseArgs()

_logger.info('%s: sleeping %d seconds' % (progName, args.seconds))
time.sleep(args.seconds)

_logger.info('%s: exiting with status %d' % (progName, args.exitCode))
sys.exit(args.exitCode)
