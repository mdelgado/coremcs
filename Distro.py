'''
Created on Mar 20, 2012

@author: Rich Plevin
@author: Sam Fendell

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''

import re
import math
from os import path
from inspect import getargspec
from itertools import starmap
from collections import defaultdict
from abc import ABCMeta, abstractmethod
from scipy.stats import lognorm, triang, uniform, norm, rv_discrete

from . import log
from .error import CoreMcsUserError, CoreMcsSystemError
from .CorrelationDef import CorrelationDef
from .Constants import (APPLY_MODIFIER, DISCRETE_SUFFIX, VALID_TARGETS,
                        DICT_SEPARATOR, DISCRETE_ENTRY_SEPARATOR, DICT_REGEX)
from .DiscreteDist import DiscreteDist
from .util import fileReader, BaseSpecError, checkSuffix, getOptionalArgs

_logger = log.getLogger(__name__)

class DistributionSpecError(BaseSpecError):
    pass

class DiscreteDistributionSpecError(BaseSpecError):
    pass


class DistroGen(object):
    '''
    Stores information required to generate a Distro instance from an argDict
    '''

    # Store a dict of our instances internally
    instances = {}

    @classmethod
    def signature(cls, distName, keywords):
        '''
        Makes a unique signature for a distribution type out of its name
        and a collection of argument names.
        '''
        lst = list(keywords)
        lst.append('#' + distName.lower())  # assures that distname doesn't overlap with any of the argument names
        return frozenset(lst)

    @classmethod
    def generator(cls, sig):
        return cls.instances.get(sig, None)

    def __init__(self, distName, func):
        self.name = distName
        self.func = func
        self.sig  = DistroGen.signature(distName, getargspec(func).args)
        DistroGen.instances[self.sig] = self

    def __str__(self):
        classname = type(self).__name__
        print "<%s dist=%s func=%s sig=%s>" % (classname, self.distName, self.func, self.sig)

    def makeRV(self, argDict):
        'Call the generator function with an argDict to create a frozen RV'
        return self.func(**argDict)


def parseDistroKey(key):
    '''
    Gets the name and list of dimensions from a distro key. Inverse of makeDistroKey
    '''
    s = re.split('\[', key)
    return s[0], s[1][:-1].split(',')

def makeDistroKey(name, dimensions, dropZeros=False):
    '''
    Generate a dictionary key for the variable and a list of dimension indices.
    This is a normal function because it is used by both the MatrixRV
    and ParameterSet classes. Inverse of parseDistroKey.
    '''
    if not dimensions:
        return name

    if dropZeros and all(dim == 0 for dim in dimensions):
        return name

    distroKey = name + re.sub(r'[\s\']', r'', str(dimensions))
    return distroKey

# For debugging only
def dumpDistros(distroDict):
    for key in sorted(distroDict.iterkeys()):
        subDistroDict = distroDict[key]
        for d in subDistroDict.values():
            _logger.info(d)

#
# Various ways to specify a lognormal random variable:
#
def lognormalRvForNormal(mu, sigma):
    '''
    Define a lognormal RV by the mean and stdev of the underlying Normal distribution
    '''
    return lognorm(sigma, scale=math.exp(mu))

def lognormalRv(logMean, logStd):
    '''
    Define a lognormal RV by its own mean and stdev
    '''
    logVar = float(logStd) ** 2
    mSqrd = float(logMean) ** 2
    mu = math.log(mSqrd / math.sqrt(logVar + mSqrd))
    sigma = math.sqrt(math.log(logVar / mSqrd + 1))
    return lognormalRvForNormal(mu, sigma)

def lognormalRvFor95th(lo, hi):
    '''
    Define a lognormal RV by its 95% CI.
    '''
    lo = math.log(float(lo))
    hi = math.log(float(hi))
    mu = (lo + hi) / 2.0
    sigma = (hi - mu) / 1.96  # 95th percentile of normal is (+/- 1.96) * sigma
    return lognormalRvForNormal(mu, sigma)

def logfactor(factor):
    if factor < 1.0:
        raise CoreMcsUserError("LogFactor 'factor' must be >= 1; a value of %f was given." % factor)

    return lognormalRvFor95th(1 / factor, factor)

def triangle(min, mode, max):  # @ReservedAssignment
    scale = max - min
    if scale == 0:
        raise CoreMcsUserError("Scale of triangle distribution is zero")

    c = (mode - min) / scale  # central value (mode) of the triangle
    return triang(c, loc=min, scale=scale)

def triangleRange(range):
    if range <= 0.0:
        raise CoreMcsUserError("Triangle delta must be between > 0.0; %f was given" % range)

    return triangle(-range, 0, range)

def triangleFactor(factor):
    if factor < 0.0 or factor > 1.0:
        raise CoreMcsUserError("Triangle range must be between 0.0 and 1.0; %f was given" % factor)

    return triangle(1 - factor, 1, 1 + factor)

def binary():
    return rv_discrete(name="binary", values=[(0, 1), (0.5, 0.5)])

class GridRV(object):
    '''
    Return an object that behaves like an RV in that it returns N values when
    when requested via the ppf (percent point function), though the N values are
    merely a shuffled sequence of a "gridded" range repeated to produce N values.
    No other methods of the standard RV class are implemented. This is intended
    for use in CoreMCS and derivatives only.
    '''
    def __init__(self, min, max, count):
        import numpy as np
        self.values = np.linspace(min, max, count)
        print "Generated values:", self.values


    def ppf(self, q):
        '''
        Return 'n' values from this object's list of values, repeating those values
        as many times as necessary to produce 'n' values, where 'n' is the length of
        the percentile list given by 'q'. (We ignore the values, though.)
        '''
        import numpy as np
        n = len(q)
        values = self.values
        assert len(values.shape) == 1, "Grid values were converted to ndarray of > 1 dimension"
        count  = values.shape[0]
        reps   = 1 if n <= count else np.ceil(float(n) / count)
        tiled  = np.tile(values, reps)[:n]
        np.random.shuffle(tiled)
        print "tiled=", tiled
        return tiled


def genDistros():
    '''
    Generate a basic set of distributions
    '''
    DistroGen('uniform', lambda min, max: uniform(loc=min, scale=(max - min)))
    DistroGen('uniform', lambda range:    uniform(loc=-range, scale=(2 * range)))

    DistroGen('normal', lambda mean, std:   norm(loc=mean, scale=std))
    DistroGen('normal', lambda mean, stdev: norm(loc=mean, scale=stdev))          # alternate spelling

    DistroGen('lognormal', lambda mean, std:     lognormalRv(mean, std))
    DistroGen('lognormal', lambda low95, high95: lognormalRvFor95th(low95, high95))
    DistroGen('lognormal', logfactor)

    # LogUniform distribution from 1/n to n, e.g., factor=3 => uniform(1/3, 3)
    DistroGen('loguniform', lambda factor: uniform(loc=1/factor, scale=(factor - 1/factor)))

    # delta=0.2 means a triangle with min, mode, max = (-0.2, 0, +0.2); for apply="add"
    DistroGen('triangle', triangleRange)    # args: delta (must be > 0)

    # range=0.2 means triangle with min, mode, max = (0.8, 1, 1.2); for apply="multiply"
    DistroGen('triangle', triangleFactor)    # args: range: must be > 0 and < 1

    DistroGen('triangle', triangle)         # args: min, mode, max

    DistroGen('binary', lambda: rv_discrete(name="binary", values=[(0, 1), (0.5, 0.5)]))

    # Gridded (non-random) sequence.
    # Returns a frozen RV-like object with a "ppf" method that returns a sequence of values
    # produced by cycling through 'count' values evenly spaced starting at 'min' and ending
    # at 'max'.
    DistroGen('grid', lambda min, max, count: GridRV(min, max, count))


class AbsBaseDistro(object):

    __metaclass__ = ABCMeta

    def __init__(self, param, target, distType, dimensions, argDict, modDict):
        self.name       = param.name
        self.dimensions = dimensions
        self.dimIndexes = []           # a list of tuples of (elementName, value)
        self.target     = target.lower() if target else None
        self.distType   = distType.lower() if distType else None
        self.argDict    = argDict
        self.modDict    = modDict
        self.param      = param
        self.isFactor   = False
        self.isDelta    = False
        self.rv         = None

        if self.target == 'index':
            pairs = zip(param.sets, dimensions)
            self.dimIndexes = list(starmap(_convertIndex, pairs))    # convert to numeric values and save in dimIndexes

        if APPLY_MODIFIER in self.modDict:
            applyAs = self.modDict[APPLY_MODIFIER]
            applyModifiers = ('add', 'mult', 'multiply', 'dir', 'direct')
            if not (applyAs in applyModifiers):
                raise DistributionSpecError("Unrecognized value (%s) for %s modifier; should be one of %s" % (applyAs, APPLY_MODIFIER, applyModifiers))
            self.isFactor = (applyAs[0:4] == 'mult')
            self.isDelta  = (applyAs      == 'add')

        # Discrete distributions don't fit into the DistroGen framework, so we need to treat them specially.
        if distType == 'discrete':
            entries = self.modDict['_entries']
            if not entries:
                raise DistributionSpecError("Not enough arguments given to discrete distribution.")
            modDict = {k[1:]: float(v) for k, v in self.modDict.iteritems() if k[1:] in getOptionalArgs(DiscreteDist.__init__)}
            self.rv = DiscreteDist(entries, **modDict)

        else:
            # When multiplying, translate "range=0.2" into min=0.8, max=1.2 (for Uniform)
            # and min=0.8, mode=0, max=1.2 for Triangle. Otherwise, (i.e., when isFactor
            # is False) use the range as is.
            if self.isFactor and 'range' in argDict:
                theRange = argDict['range']
                argDict['min'] = 1 - theRange
                argDict['max'] = 1 + theRange
                if self.distType == 'triangle':
                    argDict['mode'] = 1

                del argDict['range']

            sig = DistroGen.signature(distType, self.argDict.keys())
            gen = DistroGen.generator(sig)
            if gen is None:
                raise DistributionSpecError("Unknown distribution signature %s" % str(sig))

            self.rv = gen.makeRV(self.argDict)  # generate a frozen RV with the specified arguments

    def __str__(self):
        classname = type(self).__name__

        if not self.distType:
            return "<%s name=%s dist=None>" % (classname, self.name)

        target = self.target
        if target == "index":
            target = "[%s]" % (','.join(self.dimensions))

        args = str(self.argDict)

        return "<%s name=%s target=%s dist=%s(%s)>" % (classname, self.name, target, self.distType, args)

    def rvs(self, n):
        assert self.rv, "Random variable not set for %s object" % type(self).__name__
        return self.rv.rvs(n)

    @classmethod
    @abstractmethod
    def parseDistro(cls, param, target, distro, args):
        '''
        Creates a distribution object out of the given parameters.
        '''
        pass

    @classmethod
    @abstractmethod
    def readFile(cls, filename, paramDict):
        '''
        Read a file of distribution data associated with paramDict, a dictionary
        of parameter objects, keyed by name. The file format is application specific.
        A simple format is defined by the Distro class, below.

        Returns two items: (i) a dict keyed by short name of the distributions read
        from the file, and (ii) a list of CorrelationDef instances.
        '''
        # corrDefs = []
        # distroDict = defaultdict(list)
        # ...
        # return distroDict, corrDefs
        pass

def _convertIndex(symset, element):
    '''
    Convert a numeric or symbol set element specification to an integer.
    '''
    if element == '*':
        return 0

    try:
        return int(element)

    except ValueError:
        value = symset[element]     # e.g. <SymSet name=REG...> value for element 'usa'
        return value


class Distro(AbsBaseDistro):
    '''
    Stores a definitions of a distribution to be applied to a variable.
    '''

    @classmethod
    def parseDiscreteDistroFiles(cls, target, paramDict, discreteFiles):
        """
        Parses a discrete distribution declaration from the distro file from data contained in .ddist files.
        Target: must be cells, rows, columns, etc.
        paramDict: a dictionary of parameters, keyed by name.
        discreteFiles: list of .ddist files to read distribution data from.
        """
        distroList = []

        for disFile in discreteFiles:
            for items in fileReader(disFile, error=DiscreteDistributionSpecError, fileExtension=DISCRETE_SUFFIX):
                if len(items) < 2:
                    raise DiscreteDistributionSpecError("Missing value-frequency pairs.")
                key = items[0]
                entries = items[1:]
                distro = cls.discreteDistroLine(target, paramDict, key, entries)
                distroList.append(distro)
        return distroList

    @classmethod
    def discreteDistroLine(cls, target, paramDict, key, pairs):
        """
        Instantiate a discrete distribution from a text description
        Target must be one of cells, rows, cols, etc.
        Key is a distro key, in the form NAME[dim1,dim2,dim3...]
        Pairs is a List of text entries for the discrete distribution. Example:
        ['0:.2','8:0.1','10:0.3','100:0.4']
        """
        name, dimensions = parseDistroKey(key)
        param = paramDict[name]

        argDict = defaultdict(lambda: None)
        modDict = {}
        entries = []

        for x in pairs:
            if DISCRETE_ENTRY_SEPARATOR in x:
                p = x.partition(DISCRETE_ENTRY_SEPARATOR)
                try:
                    p1 = float(p[0])
                    p2 = float(p[2])
                except ValueError:
                    raise DistributionSpecError('Malformed pair \'%s\'.' % x)
                entries.append((p1, p2))
            elif re.match(DICT_REGEX, x):
                p = x.partition(DICT_SEPARATOR)  # handle _precision=x and _tolerance=x
                modDict[p[0]] = float(p[2])
            else:
                errorStr = "Bad discrete distro token '%s'; expected '%s' or '%s'" % (x, DISCRETE_ENTRY_SEPARATOR, DICT_SEPARATOR)
                raise DiscreteDistributionSpecError(errorStr)

        distro  = 'discrete'
        modDict['_entries'] = entries

        return cls(param, target, distro, dimensions, argDict, modDict)

    @classmethod
    def parseDistro(cls, param, target, distro, args):
        '''
        Creates a distribution object out of the given parameters.

        param is a parameter argument.
        Target is a string of 'single','cells','rows','[ind1,ind2]', etc...
        Distro is a distribution type.
        args is a list of equal-sign separated strings like
        ['foo=9','bar=-10','baz=4.2'].

        This function ensures args are
        properly formed and that target is valid.
        '''
        # Make sure target matches [x,y], where x and y are either words or *
        m = re.match(r'\A\[(\*|.+)(,(\*|.+))*\]\Z', target, flags=re.IGNORECASE)
        if m:
            target     = re.sub(r'[\[\]\s]', r'', target)  # delete brackets
            dimensions = target.split(',')  # split on commas
            target     = "index"

        else:
            dimensions = []
            if not re.match(VALID_TARGETS, target, flags=re.IGNORECASE):
                raise DistributionSpecError("Target was %r; must be one of {Rows, Cols, Cells, Single, [row,col], None}" % target)

        argDict = defaultdict(lambda: None)
        modDict = defaultdict(lambda: None)

        for argDef in args:
            p = argDef.partition(DICT_SEPARATOR)
            key = p[0]
            val = p[2]

            if not val:
                raise DistributionSpecError("Argument '%s' is missing a value", key)

            isNumber = True
            try:
                floatVal = float(val)
            except ValueError:
                isNumber = False

            if key[0] == '_':     # modifiers start with underscore
                modDict[key] = floatVal if isNumber else val
            elif isNumber:
                argDict[key] = floatVal
            else:
                raise DistributionSpecError('Malformed dict definition \'%s\'.' % argDef)

        return cls(param, target, distro, dimensions, argDict, modDict)

    @classmethod
    def readFile(cls, filename, paramDict, allowDups=False):
        '''
        Read a file of distribution data and return two items: (i) a dict keyed by short name
        of the distributions read from the file, and (ii) a list of CorrelationDef instances.
        The format of file is:
            ShortName Target DistroType key1=value1 key2=value2...
            ShortName Target Discrete   ../path/to/file.ddist  ../../other/path/to/file.ddist
            ShortName Target Discrete   5:0.3 3:0.7
        or
            Correlation VarName Coefficient              # correlation within matrix parameter
            Correlation VarName1 VarName2 Coefficient    # correlation between two scalars or identically-dimensioned matrices
        New DistroTypes can be specified using the DistroGen class.

        paramDict is a dictionary of parameters, keyed by name.
        allowDups == True causes values of distroDict to be lists of distros rather than just one
        '''
        _logger.debug('Reading in file %s' % filename)
        corrDefs = []
        distroDict = defaultdict(list if allowDups else None)

        for items in fileReader(filename, error=DistributionSpecError):
            if items[0].lower() == 'correlation':
                items = items[1:]  # remove 'Correlation', leaving the corrDef
                if not 1 < len(items) < 4:
                    raise DistributionSpecError('Invalid correlation statement.')
                corrDefs.append(CorrelationDef(*items))
                continue

            # lowercase all but name and distribution filenames (based on file extension)
            items = [items[0]] + [x if checkSuffix(x, DISCRETE_SUFFIX) else x.lower() for x in items[1:]]

            if len(items) == 2 and items[1].lower() == 'none':
                _logger.debug("Ignoring parameter %s (distro is NONE)", items[0])
                continue

            if len(items) < 3:
                raise DistributionSpecError('Malformed distro spec.')

            paramName = items[0]
            if not paramName in paramDict:
                raise DistributionSpecError('Distribution referenced undefined parameter %s' % paramName)

            param      = paramDict[paramName]
            target     = items[1]
            distroType = items[2]
            args       = items[3:]

            if target == 'none':
                continue

            # If the second token is 'discrete', parse the line as a discrete distribution
            if distroType == 'discrete':
                if checkSuffix(args[0], DISCRETE_SUFFIX):
                    files = [path.join(path.dirname(filename), arg) for arg in args]
                    ddd = cls.parseDiscreteDistroFiles(target, paramDict, files)
                    distroDict[param.name] = distroDict[param.name] + ddd
                    continue
                elif DISCRETE_ENTRY_SEPARATOR in args[0] or re.match(DICT_REGEX, args[0]):
                    distro = cls.discreteDistroLine(target, paramDict, makeDistroKey(param, []), args)
                else:
                    raise DistributionSpecError('Malformed discrete distribution.')

            else:
                distro = cls.parseDistro(param, target, distroType, args)

            if not distro:
                _logger.warn("No distribution found.")
                continue

            # Store distros in a dict by name
            if allowDups:
                # allow multiple definitions (useful in matrix cases)
                distroDict[param.name].append(distro)
            else:
                distroDict[param.name] = distro

        return distroDict, corrDefs


if __name__ == '__main__':
    rv = grid(10, 50, 7)
    values = rv.ppf(20)
    print "%d values: %s" % (len(values), values)
