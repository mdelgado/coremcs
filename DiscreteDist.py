#
# Distribution declaration files in this format for carbon stocks are found in
# ilucmc/cstocks/data/*.out
#
'''

@author: Rich Plevin
@author: Sam Fendell

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''

import numpy as np
from scipy.stats import uniform

from .error import CoreMcsUserError
from .Constants import DEFAULT_DISCRETE_PRECISION, DEFAULT_DISCRETE_TOLERANCE


class DiscreteDistError(CoreMcsUserError):
    pass


# TODO: maybe simplify using scipy.stats.rv_discrete, but loses the tolerance option.
# http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.rv_discrete.html
# from scipy import stats
# xk = np.arange(7)
# pk = (0.1, 0.2, 0.3, 0.1, 0.1, 0.0, 0.2)
# custm = stats.rv_discrete(name='custm', values=(xk, pk))
#
# import matplotlib.pyplot as plt
# fig, ax = plt.subplots(1, 1)
# ax.plot(xk, custm.pmf(xk), 'ro', ms=12, mec='r')

class DiscreteDist(object):
    """An object to represent a discrete distribution of values."""

    def __init__(self, probList, precision=DEFAULT_DISCRETE_PRECISION, tolerance=DEFAULT_DISCRETE_TOLERANCE):
        """Takes in a list of (value, probability) tuples to initiate.
        Tolerance allows for distributions with a sum of probabilities close but
        not equal to 1 due to rounding errors. precision determines how many entries
        are in the fast ppf list; ppf values will be accurate within 1/precision"""

        totalProb = sum([x[1] for x in probList])
        if not (1 - tolerance <= totalProb <= 1 + tolerance):
            raise DiscreteDistError('Sum of probabilities != 1 (sum=%f). Try setting the tolerance higher.' % totalProb)

        self.probList = sorted(probList)
        self.values = [x[0] for x in self.probList]
        self.probs = [x[1] / totalProb for x in self.probList]

        # Initialize the lookup table for fast ppf
        self.precision = precision
        self.fastPPF = np.zeros(precision)

        index = 0
        cumProb = 0
        for i in range(precision):
            if i / float(precision) > self.probs[index] + cumProb:
                cumProb += self.probs[index]
                index += 1
            self.fastPPF[i] = self.values[index]

    def __eq__(self, comp):
        return self.values == comp.values and self.probs == comp.probs and self.precision == comp.precision

    def ppf(self, percentiles):
        """
        This ppf function behaves differently than those for continuous
        distributions. In this discrete case, we just want to split the
        range from 0 to 1 so that each discrete value has the assigned
        probability. This way when the LHS function passes in a vector of
        N percentiles, the values returned will occur in the correct proportions.
        The percentiles parameter must be 'array-like' to match (some) of the
        behavior of scipy.stats.rv_continuous.ppf
        """
        if not reduce(lambda x, y: x and 0 < y < 1, percentiles):
            raise DiscreteDistError('Percentiles must all be > 0 and < 1')

        return map(lambda x: self.fastPPF[int(x * self.precision)], percentiles)

    def rvs(self, n=1):
        """Returns one or more random values, according to the RV's distribution."""
        return self.ppf(uniform.rvs(size=n))
