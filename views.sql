drop view status;
create view status as
  select s."simId", e."expName", status, count(*) from run r, experiment e, sim s
  where s."simId" = r."simId" and e."expId" = r."expId"
  group by s."simId", e."expName", status order by s."simId", e."expName", status;
