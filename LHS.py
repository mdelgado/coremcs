'''
Created on Dec 20, 2012

@author: Rich Plevin
@author: Sam Fendell

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.

Implements the Latin Hypercube Sampling technique as described by Iman and Conover, 1982,
including correlation control both for no correlation or for a specified correlation
matrix for the sampled parameters.

Heavily modified from http://nullege.com/codes/show/src@m@o@model-builder-HEAD@Bayes@lhs.py
'''

import numpy as np
from scipy import stats
from pandas import DataFrame


def rankCorrCoef(m):
    '''
    Take a 2-D array of values and produce a array of rank correlation
    coefficients representing the rank correlation among the columns.
    '''
    dummy, cols = m.shape
    corrCoef = np.zeros((cols, cols))  # @UndefinedVariable

    for i in range(cols):
        corrCoef[i, i] = 1.  # All columns are perfectly correlated with themselves
        for j in range(i + 1, cols):
            corr = stats.spearmanr(m[:, i], m[:, j])[0]
            corrCoef[i, j] = corrCoef[j, i] = corr

    return corrCoef


def genRankValues(params, trials, corrMat):
    '''
    Generate a data set of 'trials' ranks for 'params'
    parameters that obey the given correlation matrix.

    params: integer denoting number of parameters.

    trials: integer denoting number of trials.

    corrMat: rank correlation matrix for parameters.
    corrMat[i,j] denotes the rank correlation between parameter
    i and j.

    Output is a matrix with 'trials' rows and 'params' columns.
    The i'th column represents the ranks for the i'th parameter.

    So an input with params=3 and trials=6 might output:

    [[1,4,6],
     [2,3,5],
     [4,1,3],
     [6,5,2],
     [5,2,1],
     [3,6,4]]
    '''
    # Create van der Waarden scores
    strata = np.arange(1.0, trials + 1) / (trials + 1)
    vdwScores = stats.norm().ppf(strata)

    S = np.zeros((trials, params))
    for i in xrange(params):
        np.random.shuffle(vdwScores)
        S[:, i] = vdwScores

    P = np.linalg.cholesky(corrMat)

    E = rankCorrCoef(S)
    Q = np.array(np.linalg.cholesky(E))
    final = np.dot(np.dot(S, np.linalg.inv(Q).T), P.T)

    ranks = np.zeros((trials, params), dtype='i')
    for i in xrange(params):
        ranks[:, i] = stats.rankdata(final[:, i])

    return ranks


def getPercentiles(trials=100):
    '''
    Generate a list of 'trials' values, one from each of 'trials' equal-size
    segments from a uniform distribution. These are used with an RV's ppf
    (percent point function = inverse cumulative function) to retrieve the
    values for that RV at the corresponding percentiles.
    '''
    segmentSize = float(1. / trials)
    points = stats.uniform.rvs(size=trials) * segmentSize + np.arange(trials) * segmentSize  # @UndefinedVariable
    return points


def lhs(paramList, trials, corrMat=None, asDataFrame=False):
    '''
    Produce an array of 'trials' rows of values for the given parameter list,
    respecting the correlation matrix 'corrMat' if one is specified, using Latin
    Hypercube (stratified) sampling.

    paramList: List of rv-like objects representing parameters. Only requirement on
    parameter objects is that they must implement the ppf function.

    trials: number of trials to generate for each parameter.

    corrMat: a numpy matrix representing the correlation between the parameters.
    corrMat[i,j] should give the correlation between the i'th and j'th entries of
    paramlist.

    Outputs a matrix of 'trials' rows and len('paramlist') columns, each column representing
    samples taken from a parameter. The values in the i'th column are drawn from the ppf
    function of the i'th parameter from paramList, and each columns i and j are rank correlated
    according to corrMat[i,j].

    '''
    ranks = genRankValues(len(paramList), trials, corrMat) if corrMat is not None else None

    samples = np.zeros((trials, len(paramList)))  # @UndefinedVariable

    for i, param in enumerate(paramList):
        values = param.ppf(getPercentiles(trials))  # extract values from the RV for these percentiles

        if corrMat is None:
            np.random.shuffle(values)  # randomize the stratified samples @UndefinedVariable
        else:
            indices = ranks[:, i] - 1  # make them 0-relative
            values = values[indices]  # reorder to respect correlations

        samples[:, i] = values

    return DataFrame(samples) if asDataFrame else samples
